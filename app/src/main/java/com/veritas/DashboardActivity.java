package com.veritas;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.veritas.bean.NotificationBean;
import com.veritas.constants.Constants;
import com.veritas.fragment.ChangePassFragment;
import com.veritas.fragment.DashboardFragment;
import com.veritas.fragment.NotificationListFragment;
import com.veritas.utils.Pref;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NotificationListFragment.OnListFragmentInteractionListener {

    Dialog dialog;
    Toolbar mToolbar;
    FragmentManager mFrgFragmentManager;
    NavigationView navigationView;
    int selectPosition= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);

        TextView textView = (TextView)headerLayout.findViewById(R.id.tv_username);
        textView.setText(Pref.getValue(this, Pref.KEY_FIRSTNAME, "")+" "+Pref.getValue(this, Pref.KEY_LASTNAME, ""));


        mFrgFragmentManager = getSupportFragmentManager();

        Fragment fragment = DashboardFragment.newInstance();
        mFrgFragmentManager.beginTransaction().replace(R.id.id_fragment_holder, fragment, Constants.FRAG_TAG_DASHBOARD).commit();
        setTitle(getResources().getString(R.string.menu_dash));

//        navigationView.getMenu().performIdentifierAction(R.id.nav_dash, 1);
//        navigationView.getMenu().getItem(0).setChecked(true);

        if(getIntent().getExtras() != null)
        {
            selectPosition = getIntent().getIntExtra("selectView", 0);
        }
        int id;
        switch(selectPosition)
        {
            case 0: id = R.id.nav_dash;
                break;
            case 2: id = R.id.nav_notif;
                break;
            default: id = R.id.nav_dash;
                break;

        }
        navigationView.getMenu().performIdentifierAction(id, selectPosition);
        navigationView.getMenu().getItem(selectPosition).setChecked(true);
    }
    public void initDrawer()
    {
        navigationView.getMenu().performIdentifierAction(R.id.nav_dash, 0);
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String title = "";
        Bundle args = new Bundle();
        Fragment fragment = null;

        if (id == R.id.nav_dash) {
            // Handle the camera action
            title = item.getTitle().toString();
            fragment = DashboardFragment.newInstance();
            mFrgFragmentManager.beginTransaction().replace(R.id.id_fragment_holder, fragment, Constants.FRAG_TAG_DASHBOARD).commit();
            setTitle(title);
        } /*else if (id == R.id.nav_mycalls) {
            title = item.getTitle().toString();
            setTitle(title);
            fragment = CallListFragment.newInstance("total_open_request");
            mFrgFragmentManager.beginTransaction().replace(R.id.id_fragment_holder, fragment, Constants.FRAG_TAG_CALLLIST).commit();
        } */else if (id == R.id.nav_cp) {
            title = item.getTitle().toString();
            fragment = ChangePassFragment.newInstance();
            mFrgFragmentManager.beginTransaction().replace(R.id.id_fragment_holder, fragment, Constants.FRAG_TAG_CHANGEPASS).commit();
            setTitle(title);
        } else if (id == R.id.nav_notif) {
            title = item.getTitle().toString();
            setTitle(title);
            fragment = NotificationListFragment.newInstance(0);
            mFrgFragmentManager.beginTransaction().replace(R.id.id_fragment_holder, fragment, Constants.FRAG_TAG_NOTIF_LIST).commit();
        } else if (id == R.id.nav_logout) {

            Pref.openPref(DashboardActivity.this);
            Pref.clearData(DashboardActivity.this, Pref.KEY_USERID);
            Pref.clearData(DashboardActivity.this, Pref.KEY_AUTHTOKEN);
            Pref.clearData(DashboardActivity.this, Pref.KEY_FIRSTNAME);
            Pref.clearData(DashboardActivity.this, Pref.KEY_LASTNAME);

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            DashboardActivity.this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void updateActionBar(String title){
        mToolbar.removeAllViews();
        mFrgFragmentManager = getSupportFragmentManager();

        mToolbar.setTitle(title);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        invalidateOptionsMenu();
    }

    @Override
    public void onListFragmentInteraction(NotificationBean item) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("System out", "onactivityResult_dashboard");
        DashboardFragment fragmentDashboard= (DashboardFragment)mFrgFragmentManager.findFragmentByTag(Constants.FRAG_TAG_DASHBOARD);
        fragmentDashboard.onRefresh();
    }
}
