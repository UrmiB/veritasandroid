package com.veritas;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veritas.adapter.CommentAdapter;
import com.veritas.bean.CommentBean;
import com.veritas.constants.URLConstant;
import com.veritas.http.HttpRequest;
import com.veritas.utils.DividerItemDecoration;
import com.veritas.utils.JSONHandler;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddCommentActivity extends AppCompatActivity {

    Toolbar mToolbar;
    TextView txtViewComment, txtNoRecord;
    RecyclerView recyclerView;
    EditText edtComment;
    TextInputLayout il_comment;
    Button btnAddComment, btnViewMore;
    CoordinatorLayout coordinatorLayout;
    CommentAdapter adapter;
    LinearLayout linearComment;

    /**
     * Variables for load more data on list scroll down
     */
    private HttpRequest mAuthTask = null;
    private boolean loading = true;
    private boolean hasMore = true;
    private int mAPIPage = 1, totalCount;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;

    String callRequestId="";

    ArrayList<CommentBean> commentList = new ArrayList<CommentBean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        if(getIntent().getExtras() != null)
        {
            callRequestId = getIntent().getStringExtra("callRequestId");
        }
        initView();
        initToolbar();

//        bindData();
        callApi(mAPIPage);

    }
    /**
     * Initialize View component
     * */
    private void initView()
    {
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinate) ;
        txtViewComment = (TextView)findViewById(R.id.txtViewComment);
        txtViewComment.setTag("expand");
        linearComment = (LinearLayout)findViewById(R.id.linearComment);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        txtViewComment.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
            @Override
            public void onClick(View v) {
                Utils.ButtonClickEffect(txtViewComment);
                if(v.getTag().toString().equalsIgnoreCase("collapse") )
                {
//                    linearComment.setVisibility(View.VISIBLE);
                    Utils.expand(linearComment);
                    txtViewComment.setTag("expand");
                    txtViewComment.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_expand_less_black_24dp, 0);
                }
                else
                {
                    Utils.collapse(linearComment);
//                    linearComment.setVisibility(View.GONE);
                    txtViewComment.setTag("collapse");
                    txtViewComment.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_expand_more_black_24dp, 0);
                }
            }
        });
        edtComment = (EditText)findViewById(R.id.edtComment);
        il_comment = (TextInputLayout)findViewById(R.id.il_comment);
        btnAddComment = (Button)findViewById(R.id.btnAddComment);
        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCommentClick();
            }
        });

        txtNoRecord = (TextView)findViewById(R.id.txtNoRecord);
        btnViewMore = (Button)findViewById(R.id.btnViewMore);
        btnViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.ButtonClickEffect(btnViewMore);
                mAPIPage++;
                callApi(mAPIPage);
            }
        });

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        recyclerView.setHasFixedSize(true);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    /**
     * Initialize toolbar
     */
    private void initToolbar() {
        mToolbar.setTitle("Add Comment");
        setSupportActionBar(mToolbar);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void callApi(final int pageNumber)
    {
        if(Utils.isOnline(AddCommentActivity.this)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("call_request_id", callRequestId);
                jsonObject.put("page_count", pageNumber);
                jsonObject.put("auth_token", Pref.getValue(AddCommentActivity.this, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(AddCommentActivity.this, URLConstant.GET_COMMENT_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask=null;
                        try {
                            // Parse Api Response
                            if(response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    parseData(jResponse);
                                } else if (jResponse.opt("status").equals("error")) {
                                    if(pageNumber == 1)
                                    {
                                        recyclerView.setVisibility(View.GONE);
                                        txtNoRecord.setVisibility(View.VISIBLE);
                                        txtNoRecord.setText(jResponse.optString("message"));
                                    }
                                }
                            }
                            else
                                Utils.showSnackbar(AddCommentActivity.this, getResources().getString(R.string.error_server), coordinatorLayout);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Utils.showSnackbar(AddCommentActivity.this, getResources().getString(R.string.error_no_network), coordinatorLayout);
        }
    }

    private void parseData(JSONObject jResponse) {
        try {
            pastVisiblesItems = commentList.size();
            totalCount = Integer.parseInt(jResponse.getString("total_records"));
            JSONArray mJsonArray = jResponse.getJSONArray("comments");
            for (int i=0; i<mJsonArray.length(); i++)
            {
                JSONObject jsonObject = mJsonArray.getJSONObject(i);
                commentList.add((CommentBean) new JSONHandler()
                        .parse(jsonObject.toString(), CommentBean.class,
                                "com.veritas.bean"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        bindData();
    }

    private void bindData()
    {
        if (commentList.isEmpty()) {
           recyclerView.setVisibility(View.GONE);
            txtNoRecord.setVisibility(View.VISIBLE);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
            txtNoRecord.setVisibility(View.GONE);

            adapter = new CommentAdapter(AddCommentActivity.this, commentList);
            recyclerView.setAdapter(adapter);
            recyclerView.scrollToPosition(pastVisiblesItems);

            if(totalCount > commentList.size())
            {
                btnViewMore.setVisibility(View.VISIBLE);
            }
            else
            {
                btnViewMore.setVisibility(View.GONE);
            }
        }

//        loading = true;
//        recyclerView.scrollToPosition(pastVisiblesItems);


    }
    /**
     * Add Comment button click event
     * */
    private void addCommentClick()
    {
        Utils.ButtonClickEffect(btnAddComment);
        if(edtComment.getText().toString().trim().length() > 0)
        {
            il_comment.setErrorEnabled(false);
            callAddCommentApi(edtComment.getText().toString());

        }
        else
        {
            il_comment.setErrorEnabled(true);
            il_comment.setError(getResources().getString(R.string.error_field_required));

        }
    }

    private void callAddCommentApi(String text)
    {
        if(Utils.isOnline(AddCommentActivity.this)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(AddCommentActivity.this, Pref.KEY_USERID, ""));
                jsonObject.put("comment", text);
                jsonObject.put("call_request_id", callRequestId);
                jsonObject.put("auth_token", Pref.getValue(AddCommentActivity.this, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(AddCommentActivity.this, URLConstant.ADD_COMMENT_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask=null;
                        try {
                            // Parse Api Response
                            if(response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    Utils.showSnackbar(AddCommentActivity.this, jResponse.optString("message"), coordinatorLayout);
                                    edtComment.setText("");
                                    mAPIPage = 1;
                                    commentList = new ArrayList<CommentBean>();
                                    callApi(mAPIPage);
                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(AddCommentActivity.this, jResponse.optString("message"), null);
                                }
                            }
                            else
                                Utils.showSnackbar(AddCommentActivity.this, getResources().getString(R.string.error_server), coordinatorLayout);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Utils.showSnackbar(AddCommentActivity.this, getResources().getString(R.string.error_no_network), coordinatorLayout);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    /*public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }*/
}
