package com.veritas;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veritas.constants.URLConstant;
import com.veritas.crashhandler.ExceptionHandler;
import com.veritas.http.HttpRequest;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassActivity extends AppCompatActivity {

    // UI references.
    private EditText mEt_Email;
    private View mLoginFormView;
    private LinearLayout mLl_main;
    TextView mTv_back;
    TextInputLayout il_Email;

    /**
     * Constants for set as key value
     */
    private final String USERNAME="username";
    private final String AUTH_TOKEN="auth_token";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private HttpRequest mAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        initView();
    }
    private void initView() {
        // Set up the login form.
        il_Email = (TextInputLayout)findViewById(R.id.il_email);
        mLl_main = (LinearLayout)findViewById(R.id.ll_main);
        Utils.setupOutSideTouchHideKeyboard(mLl_main, this);

        mEt_Email = (EditText) findViewById(R.id.et_email);
        mTv_back = (TextView)findViewById(R.id.tv_backToLogin);

        mTv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToLogin();
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.btn_submit_fp);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptAPICall();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);

    }
    /**
     *   Forgot password click event
     * */
    public void backToLogin()
    {
        Utils.ButtonClickEffect(mTv_back);
        ForgotPassActivity.this.finish();
    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptAPICall() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEt_Email.setError(null);

        // Store values at the time of the login attempt.
        String email = mEt_Email.getText().toString();

        boolean cancel = false;
        View focusView = null;

// Check for a valid email address.
        if(!checkUserName())
        {
            return;
        }
        /*if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }*/ else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callAPI_ForgotPassword();
        }
    }

    private boolean checkUserName()
    {
        String email = mEt_Email.getText().toString();

        if (TextUtils.isEmpty(email)) {
//            mEt_Email.setError(getString(R.string.error_field_required));
            il_Email.setErrorEnabled(true);
            il_Email.setError(getString(R.string.error_field_required));
            mEt_Email.requestFocus();
            return false;
        } else if (!Utils.isEmailValid(email)) {
//            mEt_Email.setError(getString(R.string.error_invalid_email));
            il_Email.setErrorEnabled(true);
            il_Email.setError(getString(R.string.error_invalid_email));
            mEt_Email.requestFocus();
            return false;
        }
        else {
            il_Email.setError(null);
            il_Email.setErrorEnabled(false);
            return true;
        }
    }
    private void callAPI_ForgotPassword(){
        if(Utils.isOnline(ForgotPassActivity.this)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(USERNAME, mEt_Email.getText().toString().trim());
                jsonObject.put(AUTH_TOKEN, Pref.getValue(ForgotPassActivity.this, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(ForgotPassActivity.this, URLConstant.FORGOTPASSWORD_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask=null;
                        try {
                            // Parse Api Response
                            if(response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    Utils.showSnackbar(ForgotPassActivity.this, jResponse.optString("message"), mLl_main);
                                    clearTexts();
                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(ForgotPassActivity.this, jResponse.optString("message"), null);
                                }
                            }
                            else
                                Utils.showSnackbar(ForgotPassActivity.this, getResources().getString(R.string.error_server), mLl_main);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Utils.showSnackbar(ForgotPassActivity.this, getResources().getString(R.string.error_no_network), mLl_main);
        }
    }

    private void clearTexts() {
        mEt_Email.setText("");
    }
}
