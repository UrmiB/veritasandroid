package com.veritas.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import com.veritas.DashboardActivity;
import com.veritas.R;
import com.veritas.constants.URLConstant;
import com.veritas.crashhandler.ExceptionHandler;
import com.veritas.http.HttpRequest;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // UI references.
    private EditText mEt_newPassword;
    private EditText mEt_oldPassword;
    private EditText mEt_confPassword;
    TextInputLayout mIl_newPassword, mIl_oldPassword, mIl_confPassword;
    private ScrollView mLl_main;

    /**
     * Constants for set as key value
     */
    private final String USERID="user_id";
    private final String PASSWORD="newpassword";
    private final String AUTH_TOKEN="auth_token";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private HttpRequest mAuthTask = null;
    private Context mContext;

    public ChangePassFragment() {
        // Required empty public constructor
    }

    /**
     * Use mContext factory method to create a new instance of
     * mContext fragment using the provided parameters.
     *
     * @return A new instance of fragment ChangePassFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePassFragment newInstance() {
        ChangePassFragment fragment = new ChangePassFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext=getActivity();
        // Inflate the layout for mContext fragment
        View view = inflater.inflate(R.layout.fragment_change_pass, container, false);
        initView(view);
        return view;
    }
    private void initView(View view) {
        // Set up the login form.
        mLl_main = (ScrollView)view.findViewById(R.id.ll_main);
        Utils.setupOutSideTouchHideKeyboard(mLl_main, mContext);

        mEt_oldPassword = (EditText)view. findViewById(R.id.et_password_old);
        mEt_newPassword = (EditText)view. findViewById(R.id.et_password_new);
        mEt_confPassword = (EditText)view. findViewById(R.id.et_password_conf);

        mIl_oldPassword = (TextInputLayout)view.findViewById(R.id.il_pass_old);
        mIl_newPassword = (TextInputLayout)view.findViewById(R.id.il_pass_new);
        mIl_confPassword = (TextInputLayout)view.findViewById(R.id.il_pass_conf);

        Button mEmailSignInButton = (Button)view. findViewById(R.id.btn_submit_cp);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptAPICall();
            }
        });

        Pref.openPref(mContext);

    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptAPICall() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mIl_oldPassword.setError(null);
        mIl_newPassword.setError(null);
        mIl_confPassword.setError(null);
        mIl_oldPassword.setErrorEnabled(false);
        mIl_newPassword.setErrorEnabled(false);
        mIl_confPassword.setErrorEnabled(false);

        // Store values at the time of the login attempt.
        String password_old = mEt_oldPassword.getText().toString();
        String password_new = mEt_newPassword.getText().toString();
        String password_conf = mEt_confPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password_old)) {
            mIl_oldPassword.setErrorEnabled(true);
            mIl_oldPassword.setError(getString(R.string.error_field_required));
            focusView = mEt_oldPassword;
            cancel = true;
        }else if (TextUtils.isEmpty(password_new)) {
            mIl_newPassword.setErrorEnabled(true);
            mIl_newPassword.setError(getString(R.string.error_field_required));
            focusView = mEt_newPassword;
            cancel = true;
        }else if (TextUtils.isEmpty(password_conf)) {
            mIl_confPassword.setErrorEnabled(true);
            mIl_confPassword.setError(getString(R.string.error_field_required));
            focusView = mEt_confPassword;
            cancel = true;
        }else
        if (!password_old.equalsIgnoreCase(Pref.getValue(mContext, Pref.KEY_PASSWORD, ""))) {
            mIl_oldPassword.setErrorEnabled(true);
            mIl_oldPassword.setError(getString(R.string.error_incorrect_password));
            focusView = mEt_oldPassword;
            cancel = true;
        }else
        if (!isPasswordValid(password_new)) {
            mIl_newPassword.setErrorEnabled(true);
            mIl_newPassword.setError(getString(R.string.error_invalid_password));
            focusView = mEt_newPassword;
            cancel = true;
        }else
        if (!password_new.equalsIgnoreCase(password_conf)){
            mIl_confPassword.setErrorEnabled(true);
            mIl_confPassword.setError(getString(R.string.error_password_mismatch));
            focusView = mEt_confPassword;
            cancel = true;
        }else{
            cancel = false;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callAPI_ChangePassword();
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 8;
    }
    private void callAPI_ChangePassword(){
        if(Utils.isOnline(mContext)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(USERID, Pref.getValue(mContext, Pref.KEY_USERID, ""));
                jsonObject.put(PASSWORD, mEt_newPassword.getText().toString().trim());
                jsonObject.put(AUTH_TOKEN, Pref.getValue(mContext, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(mContext, URLConstant.CHANGE_PASS_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask=null;
                        try {
                            // Parse Api Response
                            if(response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    Utils.showSnackbar(mContext, jResponse.optString("message"), mLl_main);
                                    clearTexts();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            DashboardActivity activity = (DashboardActivity)getActivity();
                                            activity.initDrawer();
                                        }
                                    },2000);
                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(mContext, jResponse.optString("message"), null);
                                }
                            }
                            else
                                Utils.showSnackbar(mContext, getResources().getString(R.string.error_server), mLl_main);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Utils.showSnackbar(mContext, getResources().getString(R.string.error_no_network), mLl_main);
        }
    }

    private void clearTexts() {
        mEt_oldPassword.setText("");
        mEt_newPassword.setText("");
        mEt_confPassword.setText("");
    }
}
