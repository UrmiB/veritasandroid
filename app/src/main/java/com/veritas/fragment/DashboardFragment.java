package com.veritas.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.veritas.CallListActivity;
import com.veritas.R;
import com.veritas.adapter.DashboardAdapter;
import com.veritas.bean.DashboardBean;
import com.veritas.bean.ErrorCodeBean;
import com.veritas.constants.URLConstant;
import com.veritas.crashhandler.ExceptionHandler;
import com.veritas.http.HttpRequest;
import com.veritas.utils.JSONHandler;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class DashboardFragment extends Fragment implements DashboardAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    // TODO: Rename parameter arguments, choose names that match

    /*UI declaration*/
    private RecyclerView mRv_callList;
    SwipeRefreshLayout swipeLayout;
    /**
     * Constants for set as key value
     */
    private final String USERID = "user_id";
    private final String AUTH_TOKEN = "auth_token";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private HttpRequest mAuthTask = null;
    private Context mContext;
    private ArrayList<DashboardBean> mDashboardList = new ArrayList<DashboardBean>();
    public static SparseArray<Boolean> mCheckedArray_type = new SparseArray<>();
    public static SparseArray<Boolean> mCheckedArray_err = new SparseArray<>();
    public static SparseArray<Boolean> mCheckedArray_status = new SparseArray<>();
    public static int prevSelectedType = -1;
    public static int prevSelectedErr = -1;
    public static int prevSelectedStatus = -1;

    FragmentManager mFrgFragmentManager;
    private boolean mFlag_refresh = false;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use mContext factory method to create a new instance of
     * mContext fragment using the provided parameters.
     *
     * @return A new instance of fragment ChangePassFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        mFrgFragmentManager = getActivity().getSupportFragmentManager();
        // Inflate the layout for mContext fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        // Set up the dashboard view.
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        // Set the adapter
        mRv_callList = (RecyclerView) view.findViewById(R.id.layout_call_list);
        mRv_callList.setLayoutManager(new LinearLayoutManager(mContext));

        callAPI_Dashboard();
    }

    public void callAPI_Dashboard() {
        if (Utils.isOnline(mContext)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(USERID, Pref.getValue(mContext, Pref.KEY_USERID, ""));
                jsonObject.put(AUTH_TOKEN, Pref.getValue(mContext, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(mContext, URLConstant.DASHBOARD_URL,
                        jsonObject.toString(), !mFlag_refresh, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            // Parse Api Response
                            if (response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    parseData(jResponse);
                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(mContext, jResponse.optString("message"), null);
                                }
                                parseData(jResponse);
                            } else {
                                Utils.showSnackbar(mContext, getResources().getString(R.string.error_server), mRv_callList);
                                parseData(null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showSnackbar(mContext, getResources().getString(R.string.error_no_network), mRv_callList);
        }
    }

    private void parseData(JSONObject jResponse) {
        if (jResponse != null) {
            try {
                mDashboardList.add((DashboardBean) new JSONHandler()
                        .parse(jResponse.toString(), DashboardBean.class,
                                "com.veritas.bean"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        /*Store service types for use in call list filter*/
            ArrayList<String> mTypeKey = new ArrayList<String>();
            ArrayList<String> mTypeValue = new ArrayList<String>();
            JSONObject jsonObject_services = jResponse.optJSONObject("filter_services");
            if (jsonObject_services != null && jsonObject_services.length() != 0) {
                Iterator<String> stringIterator = jsonObject_services.keys();
                try {
                    int i = 0;
                    while (stringIterator.hasNext()) {
                        String key = stringIterator.next();
                        mTypeKey.add(key);
                        mTypeValue.add(jsonObject_services.getString(key));
                        mCheckedArray_type.put(i, false);
                        i++;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Pref.setBeanValue(mContext, "filter_type_key_obj", mTypeKey);
                Pref.setBeanValue(mContext, "filter_type_value_obj", mTypeValue);
            }


        /*Store error codes for use in call list filter*/
            ArrayList<String> mErrKey = new ArrayList<String>();
            ArrayList<String> mErrValue = new ArrayList<String>();
            ArrayList<ErrorCodeBean> errorCodeList = new ArrayList<ErrorCodeBean>();

            JSONArray jsonArray_err = jResponse.optJSONArray("filter_error_code");
//            JSONObject jsonObject_err = jResponse.optJSONObject("filter_error_code");
            if (jsonArray_err != null && jsonArray_err.length() != 0) {
//                Iterator<String> stringIterator_err = jsonObject_err.keys();
                try {
                    for (int i = 0; i < jsonArray_err.length(); i++) {

                        JSONObject jsonObj = jsonArray_err.getJSONObject(i);

                        ErrorCodeBean bean = new ErrorCodeBean();
                        mErrKey.add(jsonObj.optString("id"));
                        mErrValue.add(jsonObj.getString("error_name"));


                        //-----------modified ---------
                        bean.setID(jsonObj.optString("id"));
                        bean.setErrorName(jsonObj.optString("error_name"));
                        bean.setType(jsonObj.optString("type"));
                        errorCodeList.add(bean);

                        mCheckedArray_err.put(i, false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //-----------modified ---------
                Collections.sort(errorCodeList, new Comparator<ErrorCodeBean>() {
                    @Override
                    public int compare(ErrorCodeBean o1, ErrorCodeBean o2) {
                        return o1.getErrorName().compareTo(o2.getErrorName());
                    }
                });
                Pref.setBeanValue(mContext, "sorted_err_list", errorCodeList);

                Pref.setBeanValue(mContext, "filter_err_key_obj", mErrKey);
                Pref.setBeanValue(mContext, "filter_err_value_obj", mErrValue);
            }

            List<String> mStatusList = Arrays.asList(mContext.getResources().getStringArray(R.array.status_list));
            for (int i = 0; i < mStatusList.size(); i++) {
                mCheckedArray_status.put(i, false);
            }
        }

        if (mFlag_refresh)

        {
            mFlag_refresh = false;
            swipeLayout.setRefreshing(false);
            mRv_callList.setEnabled(true);
        }

        if (mDashboardList != null && mDashboardList.size() > 0)

        {
            DashboardAdapter adapter = new DashboardAdapter(mContext, mDashboardList, mFrgFragmentManager);
            mRv_callList.setAdapter(adapter);
            adapter.setOnItemClickListener(this);
        }

    }

    @Override
    public void onItemClick(View view, int position) {
        String mType = "";
        switch (position) {
            case 0:
                mType = mContext.getResources().getString(R.string.type_assigned_cur_day);
                break;
            case 1:
                mType = mContext.getResources().getString(R.string.type_resolved_cur_day);
                break;
            case 2:
                mType = mContext.getResources().getString(R.string.type_total_open);
                break;
        }
        Log.d("System out", "Call Type::: " + mType);
        Intent intent = new Intent(mContext, CallListActivity.class);
        intent.putExtra("call_type", mType);
        getActivity().startActivityForResult(intent, 0);
//        mContext.startActivity(intent);
    }

    @Override
    public void onRefresh() {
        com.veritas.utils.Log.print("......................onRefresh...........................");
        mFlag_refresh = true;
        mDashboardList.clear();
        mDashboardList = new ArrayList<>();
        mRv_callList.setEnabled(false);
        callAPI_Dashboard();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("System out", "Call Type::: " + resultCode);
    }
}
