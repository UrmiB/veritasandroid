package com.veritas.fragment;


import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.graphics.Palette;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.veritas.R;

/**
 * A simple {@link DialogFragment} subclass.
 * Use the {@link ImageDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "image_url";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    DisplayMetrics metrics = new DisplayMetrics();

    public ImageDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ImageDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImageDialogFragment newInstance(String param1, String param2) {
        ImageDialogFragment fragment = new ImageDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        if (getDialog() == null)
            return;

//        getDialog().getWindow().setLayout(metrics.widthPixels, (metrics.heightPixels/15)*10);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_image_dialog, new RelativeLayout(getActivity()), false);

//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        String mUrl = mParam1;//"https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAAS1AAAAJGI5MWJhZjk4LTg1NTItNGQ2NC05MjVmLTU0NzljODQxZDM4Mw.png";

//        ViewCompat.setTransitionName(view.findViewById(R.id.fr_img), mUrl);
//        getActivity().supportPostponeEnterTransition();
        android.util.Log.d("System out", "Image: "+mUrl);
        final ImageView image = (ImageView)view.findViewById(R.id.image);
        final ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.loading_spinner);

        final RelativeLayout relative = (RelativeLayout)view.findViewById(R.id.fr_img);

        final Point displySize = getDisplaySize(metrics);
        final int size = (int) Math.ceil(Math.sqrt(displySize.x * displySize.y));



        Picasso.with(getActivity()).load(mUrl)
                .into(image, new Callback() {
            @Override public void onSuccess() {
                progressBar.setVisibility(View.GONE);
                Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
				Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette palette) {
                        applyPalette(palette);
                    }
                });
                /*Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette palette) {
                        applyPalette(palette);
                    }
                });*/
            }

            @Override public void onError() {
                progressBar.setVisibility(View.GONE);
                android.util.Log.d("System out", "On Error");
                image.setImageResource(R.drawable.logo);
                relative.setBackgroundColor(getResources().getColor(android.R.color.white));
            }
        });
        Dialog builder = new Dialog(getActivity(), R.style.DialogTheme);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        builder.getWindow().setLayout(metrics.widthPixels/2, metrics.heightPixels/2);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setContentView(view);
        builder.setCancelable(false);
        return builder;
    }
    private void applyPalette(Palette palette) {
        getActivity().supportStartPostponedEnterTransition();
    }

    public Point getDisplaySize(DisplayMetrics displayMetrics) {
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        return new Point(width, height);
    }

}
