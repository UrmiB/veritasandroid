package com.veritas.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veritas.R;
import com.veritas.adapter.MyNotificationRecyclerViewAdapter;
import com.veritas.bean.NotificationBean;
import com.veritas.constants.URLConstant;
import com.veritas.http.AssetsReader;
import com.veritas.http.HttpRequest;
import com.veritas.utils.JSONHandler;
import com.veritas.utils.Log;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class NotificationListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private OnListFragmentInteractionListener mListener;
    ArrayList<NotificationBean> mNotificationBeanArrayList = new ArrayList<>();
    private static final String LOCATION_JSON_FILE="json/notification.txt";


    /*UI declaration*/
    private RecyclerView mRv_notif;
    private TextView mTv_nodata;
    private View mMainView;
    LinearLayoutManager mLayoutManager_list;

    /**
     * Keep track of the call request task to ensure we can cancel it if requested.
     */
    private HttpRequest mAuthTask = null;
    private int mPage=1;
    private Context mContext;
    
    /**
     * Variables for load more data on list scroll down
     */
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    FragmentManager mFrgFragmentManager;
    /**
     * Constants for set as key value
     */
    private final String USERID="user_id";
    private final String PAGE_COUNT="page_count";
    private final String AUTH_TOKEN="auth_token";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NotificationListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NotificationListFragment newInstance(int columnCount) {
        NotificationListFragment fragment = new NotificationListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for mContext fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        mContext=getActivity();
        mFrgFragmentManager = getActivity().getSupportFragmentManager();

        mMainView=view;
        initView(view);

        return view;
    }
    private void initView(View view) {
        // Set up the call list view.
        mRv_notif = (RecyclerView) view.findViewById(R.id.rv_notif_list);
        mLayoutManager_list = new LinearLayoutManager(mContext,
                LinearLayout.VERTICAL, false);
        mRv_notif.setLayoutManager(mLayoutManager_list);
        mTv_nodata = (TextView)view.findViewById(R.id.tv_nodata);

        callAPI_RequestCall();
//        callOffline();
    }

    private void callAPI_RequestCall(){
        if(Utils.isOnline(mContext)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(USERID, Pref.getValue(mContext, Pref.KEY_USERID, ""));
                jsonObject.put(PAGE_COUNT, mPage);
                jsonObject.put(AUTH_TOKEN, Pref.getValue(mContext, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(mContext, URLConstant.NOTIFICATION_LIST_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask=null;
                        try {
                            // Parse Api Response
                            if(response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    parseData(jResponse);
                                } else if (jResponse.opt("status").equals("error")) {
                                    if(mPage == 1)
                                    {
                                        mRv_notif.setVisibility(View.GONE);
                                        mTv_nodata.setVisibility(View.VISIBLE);
                                        mTv_nodata.setText(jResponse.optString("message"));
                                    }
                                }else if (jResponse.opt("status").equals("fail")) {
                                    if(mPage == 1)
                                    {
                                        mRv_notif.setVisibility(View.GONE);
                                        mTv_nodata.setVisibility(View.VISIBLE);
                                        mTv_nodata.setText(getResources().getString(R.string.error_nodata));
                                    }
                                }
                            }
                            else
                                Utils.showSnackbar(mContext, getResources().getString(R.string.error_server), mMainView);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Utils.showSnackbar(mContext, getResources().getString(R.string.error_no_network), mMainView);
        }
    }

    private void parseData(JSONObject jResponse) {
        try {
            JSONArray mJsonArray = jResponse.getJSONArray("notification");
            for (int i=0; i<mJsonArray.length(); i++)
            {
                JSONObject jsonObject = mJsonArray.getJSONObject(i);
                mNotificationBeanArrayList.add((NotificationBean) new JSONHandler()
                        .parse(jsonObject.toString(), NotificationBean.class,
                                "com.veritas.bean"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        setData();
    }
    private void setData(){
        if (mNotificationBeanArrayList.isEmpty()) {
            mRv_notif.setVisibility(View.GONE);
            mTv_nodata.setVisibility(View.VISIBLE);
        }else {
            mRv_notif.setVisibility(View.VISIBLE);
            mTv_nodata.setVisibility(View.GONE);

            mRv_notif.setAdapter(new MyNotificationRecyclerViewAdapter(mNotificationBeanArrayList, mListener));

            loading = true;
            mRv_notif.scrollToPosition(pastVisiblesItems);

            mRv_notif.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {

                }
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView,
                                                 int newState) {
                    // TODO Auto-generated method stub
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {

                        visibleItemCount = mLayoutManager_list.getChildCount();
                        totalItemCount = mLayoutManager_list.getItemCount();
                        pastVisiblesItems = mLayoutManager_list.findFirstVisibleItemPosition();

                        if (loading && totalItemCount>=10)
                        {
                            if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                            {
                                android.util.Log.e("System out", "\nvisibleItemCount::: "+visibleItemCount+"\npastVisiblesItems::: "+pastVisiblesItems
                                        +"\ntotalItemCount::: "+totalItemCount);
                                loading = false;
                                Log.print("...", "Last Item Wow !");
                                //Do pagination.. i.e. fetch new data
                                mPage++;
                                callAPI_RequestCall();
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(NotificationBean item);
    }

    public String loadJSONFromAsset() {

        AssetsReader assetsReader = new AssetsReader(mContext);
        String fileResponse = assetsReader.getAssetsJsonData(LOCATION_JSON_FILE);

        return fileResponse;
    }
    public void callOffline(){
        try {
            String locationString = loadJSONFromAsset();
            Log.print("System out", "OFFLINE LOC ::::  "+locationString);
            JSONObject jsonObject1 = new JSONObject(locationString);
            parseData(jsonObject1);
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e1){
            e1.printStackTrace();
        }
    }
}
