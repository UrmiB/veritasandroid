package com.veritas.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veritas.CallDetailActivity;
import com.veritas.ChangeStatusActivity;
import com.veritas.R;
import com.veritas.adapter.MyCallListRecyclerViewAdapter;
import com.veritas.bean.CallListBean;
import com.veritas.constants.URLConstant;
import com.veritas.crashhandler.ExceptionHandler;
import com.veritas.custom.BottomSheetDialogView;
import com.veritas.http.HttpRequest;
import com.veritas.utils.JSONHandler;
import com.veritas.utils.Log;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 */
public class CallListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, MyCallListRecyclerViewAdapter.OnStatusClickListener {

    // TODO: Customize parameter argument names
    private static final String ARG_CALL_TYPE = "call_type";

    /*UI declaration*/
    private RelativeLayout mRl_main;
    private RecyclerView mRv_callList;
    private TextView mTv_nodata;
    SwipeRefreshLayout swipeLayout;
    LinearLayoutManager mLayoutManager_list;
    private View mMainView;
    PopupMenu popupMenu;
    /**
     * Constants for set as key value
     */
    private final String USERID = "user_id";
    private final String CALL_TYPE = "call_type";
    private final String FILTER_BY_CALL_REQUEST_NO = "filter_by_call_request_no";
    private final String FILTER_BY_SERVICE_TYPE = "filter_by_service_type";
    private final String FILTER_BY_ATM_MACHINE_ID = "filter_by_atm_machine_id";
    private final String FILTER_BY_ERROR_ID = "filter_by_error_id";
    private final String FILTER_BY_STATUS = "filter_by_status";
    private final String PAGE_COUNT = "page_count";
    private final String AUTH_TOKEN = "auth_token";
    /**
     * Keep track of the call request task to ensure we can cancel it if requested.
     */
    private HttpRequest mAuthTask = null;
    private String mCallType = "";
    private int mPage = 1;
    private Context mContext;
    private ArrayList<CallListBean> mCallList = new ArrayList<CallListBean>();
    private boolean mFlag_refresh = false;
    private String mReqJsonArray = "";
    private String mAtmJsonArray = "";
    //    private JSONArray mReqJsonArray = new JSONArray();
    private JSONArray mTypeJsonArray = new JSONArray();
    //    private JSONArray mAtmJsonArray = new JSONArray();
    private JSONArray mErrJsonArray = new JSONArray();
    private JSONArray mStatusJsonArray = new JSONArray();
    private SparseArray<Boolean> mCheckedArray = new SparseArray<>();


    /**
     * Variables for load more data on list scroll down
     */
    private boolean loading = true;
    private boolean hasMore = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    FragmentManager mFrgFragmentManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CallListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CallListFragment newInstance(String callType) {
        CallListFragment fragment = new CallListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CALL_TYPE, callType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));
        if (getArguments() != null) {
            mCallType = getArguments().getString(ARG_CALL_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        mFrgFragmentManager = getActivity().getSupportFragmentManager();
        // Inflate the layout for mContext fragment
        View view = inflater.inflate(R.layout.fragment_calllist, container, false);

        mMainView = view;
        initView(view);

        return view;
    }

    private void initView(View view) {
        // Set up the call list view.
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mRl_main = (RelativeLayout) view.findViewById(R.id.rl_mainList);
        mRv_callList = (RecyclerView) view.findViewById(R.id.list);
        mLayoutManager_list = new LinearLayoutManager(mContext,
                LinearLayout.VERTICAL, false);
        mRv_callList.setLayoutManager(mLayoutManager_list);
        mTv_nodata = (TextView) view.findViewById(R.id.tv_nodata);

        callAPI_RequestCall();

        mCheckedArray.put(0, false);
        mCheckedArray.put(1, false);
        mCheckedArray.put(2, false);
        mCheckedArray.put(3, false);
        mCheckedArray.put(4, false);
    }

    private void callAPI_RequestCall() {
        if (Utils.isOnline(mContext)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(USERID, Pref.getValue(mContext, Pref.KEY_USERID, ""));
                jsonObject.put(CALL_TYPE, mCallType);
                jsonObject.put(FILTER_BY_CALL_REQUEST_NO, mReqJsonArray.toString());
                jsonObject.put(FILTER_BY_SERVICE_TYPE, mTypeJsonArray);
                jsonObject.put(FILTER_BY_ATM_MACHINE_ID, mAtmJsonArray.toString());
                jsonObject.put(FILTER_BY_ERROR_ID, mErrJsonArray);
                jsonObject.put(FILTER_BY_STATUS, mStatusJsonArray);
                jsonObject.put(PAGE_COUNT, mPage);
                jsonObject.put(AUTH_TOKEN, Pref.getValue(mContext, Pref.KEY_AUTHTOKEN, ""));

                mAuthTask = new HttpRequest(mContext, URLConstant.CALLLIST_URL,
                        jsonObject.toString(), !mFlag_refresh, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            // Parse Api Response
                            if (response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    parseData(jResponse);
                                } else if (jResponse.opt("status").equals("error")) {
                                    if (mPage == 1) {
                                        mRv_callList.setVisibility(View.GONE);
                                        swipeLayout.setVisibility(View.GONE);
                                        mTv_nodata.setVisibility(View.VISIBLE);
                                        mTv_nodata.setText(jResponse.optString("message"));
                                    }
                                } else if (jResponse.opt("status").equals("fail")) {
                                    if (mPage == 1) {
                                        mRv_callList.setVisibility(View.GONE);
                                        swipeLayout.setVisibility(View.GONE);
                                        mTv_nodata.setVisibility(View.VISIBLE);
                                        mTv_nodata.setText(getResources().getString(R.string.error_nodata));
                                    }
                                }
                            } else
                                Utils.showSnackbar(mContext, getResources().getString(R.string.error_server), mRl_main);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showSnackbar(mContext, getResources().getString(R.string.error_no_network), mRl_main);
        }
    }

    private void parseData(JSONObject jResponse) {
        try {
            JSONArray mJsonArray = jResponse.getJSONArray("request_data");
            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject jsonObject = mJsonArray.getJSONObject(i);
                mCallList.add((CallListBean) new JSONHandler()
                        .parse(jsonObject.toString(), CallListBean.class,
                                "com.veritas.bean"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        setData();
    }

    private void setData() {
        if (mCallList.isEmpty()) {
            mRv_callList.setVisibility(View.GONE);
            swipeLayout.setVisibility(View.GONE);
            mTv_nodata.setVisibility(View.VISIBLE);
        } else {
            mRv_callList.setVisibility(View.VISIBLE);
            swipeLayout.setVisibility(View.VISIBLE);
            mTv_nodata.setVisibility(View.GONE);

            if (mFlag_refresh) {
                mFlag_refresh = false;
                swipeLayout.setRefreshing(false);
                mRv_callList.setEnabled(true);
            }

            MyCallListRecyclerViewAdapter adapter = new MyCallListRecyclerViewAdapter(mContext, mCallList, mCallType);
            mRv_callList.setAdapter(adapter);
            adapter.setOnStatusClickListener(this);

            loading = true;
            mRv_callList.scrollToPosition(pastVisiblesItems);

            mRv_callList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView,
                                                 int newState) {
                    // TODO Auto-generated method stub
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {

                        visibleItemCount = mLayoutManager_list.getChildCount();
                        totalItemCount = mLayoutManager_list.getItemCount();
                        pastVisiblesItems = mLayoutManager_list.findFirstVisibleItemPosition();

                        if (loading && totalItemCount >= 10) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                android.util.Log.e("System out", "\nvisibleItemCount::: " + visibleItemCount + "\npastVisiblesItems::: " + pastVisiblesItems
                                        + "\ntotalItemCount::: " + totalItemCount);
                                loading = false;
                                Log.print("...", "Last Item Wow !");
                                //Do pagination.. i.e. fetch new data
                                mPage++;
                                callAPI_RequestCall();
                            }
                        }
                    }
                }
            });
        }
    }

    public void showFilterPopup() {

        View menuItemView = getActivity().findViewById(R.id.action_filter); // SAME ID AS MENU ID
        popupMenu = new PopupMenu(mContext, menuItemView);
        popupMenu.inflate(R.menu.menu_filter);

        popupMenu.getMenu().getItem(0).setChecked(mCheckedArray.get(0));
        popupMenu.getMenu().getItem(1).setChecked(mCheckedArray.get(1));
        popupMenu.getMenu().getItem(2).setChecked(mCheckedArray.get(2));
        popupMenu.getMenu().getItem(3).setChecked(mCheckedArray.get(3));
        popupMenu.getMenu().getItem(4).setChecked(mCheckedArray.get(4));

        switch (mCallType) {
            case "assinged_cur_day":
                popupMenu.show();
                break;
            case "resolved_curr_day":
                popupMenu.getMenu().removeItem(R.id.menu_status);
                popupMenu.show();
                break;
            case "total_open_call":
                popupMenu.show();
                break;
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu_requestid:
                        BottomSheetDialogView.show(mContext, 1, mFrgFragmentManager);
                           /* if (item.isChecked()){
                            *//*item.setChecked(false);
                            mCheckedArray.put(0, false);*//*
                            }else{
                                item.setChecked(true);
                                mCheckedArray.put(0, true);
//                            mCheckedArray.put(1, false);
//                            mCheckedArray.put(2, false);
//                            mCheckedArray.put(3, false);
                            }*/
                        break;
                    case R.id.menu_tpe:
                        BottomSheetDialogView.show(mContext, 2, mFrgFragmentManager);
                        break;
                    case R.id.menu_atmid:
                        BottomSheetDialogView.show(mContext, 3, mFrgFragmentManager);
                            /*if (item.isChecked()){
                            *//*item.setChecked(false);
                            mCheckedArray.put(2, false);*//*
                            }else{
                                item.setChecked(true);
                                mCheckedArray.put(2, true);
//                            mCheckedArray.put(0, false);
//                            mCheckedArray.put(1, false);
//                            mCheckedArray.put(3, false);
                            }*/
                        break;
                    case R.id.menu_error:
                        BottomSheetDialogView.show(mContext, 4, mFrgFragmentManager);
                        break;
                    case R.id.menu_status:
                        BottomSheetDialogView.show(mContext, 5, mFrgFragmentManager);

                        break;
                    case R.id.menu_clear:
                        mCheckedArray.put(0, false);
                        mCheckedArray.put(1, false);
                        mCheckedArray.put(2, false);
                        mCheckedArray.put(3, false);
                        mCheckedArray.put(4, false);
                        mReqJsonArray = "";
                        mTypeJsonArray = new JSONArray();
                        mAtmJsonArray = "";
                        mErrJsonArray = new JSONArray();
                        mStatusJsonArray = new JSONArray();

                        for (int i = 0; i < DashboardFragment.mCheckedArray_type.size(); i++) {
                            DashboardFragment.mCheckedArray_type.put(i, false);
                        }
                        for (int i = 0; i < DashboardFragment.mCheckedArray_err.size(); i++) {
                            DashboardFragment.mCheckedArray_err.put(i, false);
                        }
                        for (int i = 0; i < DashboardFragment.mCheckedArray_status.size(); i++) {
                            DashboardFragment.mCheckedArray_status.put(i, false);
                        }
                        updateFilterValues(0, "clear");
                        break;
                }
                popupMenu.dismiss();
                return false;
            }
        });


    }

    public void updateFilterValues(int filterCode, String value) {

            /*mReqJsonArray = new JSONArray();
            mTypeJsonArray = new JSONArray();
            mAtmJsonArray = new JSONArray();
            mErrJsonArray = new JSONArray();*/
        switch (filterCode) {
            case 0:

                break;
            case 1:
                mReqJsonArray = "";
                if (!value.equals(""))
                    mReqJsonArray = value;
                popupMenu.getMenu().getItem(filterCode - 1).setChecked(true);
                mCheckedArray.put(0, true);
                break;
            case 2:
                mTypeJsonArray = new JSONArray();
                mTypeJsonArray.put(value);
                popupMenu.getMenu().getItem(filterCode - 1).setChecked(true);
                mCheckedArray.put(1, true);
                break;
            case 3:
                mAtmJsonArray = "";
                if (!value.equals(""))
                    mAtmJsonArray = value;
                popupMenu.getMenu().getItem(filterCode - 1).setChecked(true);
                mCheckedArray.put(2, true);
                break;
            case 4:
                mErrJsonArray = new JSONArray();
                mErrJsonArray.put(value);
                popupMenu.getMenu().getItem(filterCode - 1).setChecked(true);
                mCheckedArray.put(3, true);
                break;
            case 5:
                mStatusJsonArray = new JSONArray();
                mStatusJsonArray.put(value);
                popupMenu.getMenu().getItem(filterCode - 1).setChecked(true);
                mCheckedArray.put(4, true);
                break;
        }
        mPage = 1;
        mCallList.clear();
        mCallList = new ArrayList<>();
        callAPI_RequestCall();
    }


    @Override
    public void onRefresh() {
        Log.print("......................onRefresh...........................");
        mFlag_refresh = true;
        mCallList.clear();
        mCallList = new ArrayList<>();
        mPage = 1;
        mRv_callList.setEnabled(false);
        callAPI_RequestCall();
    }

    @Override
    public void onButtonClick(Context context, CallListBean bean, String type) {
        Bundle bundle = new Bundle();
        bundle.putString("callRequestId", bean.getCall_request_id());
        bundle.putSerializable("callDetail", bean);
        bundle.putString("callType", mCallType);
        bundle.putString("callStatus", bean.getCall_status());
        if (type.equalsIgnoreCase("viewCall"))
            startActivityForResult(new Intent(mContext, CallDetailActivity.class).putExtras(bundle), 102);
        else
            startActivityForResult(new Intent(mContext, ChangeStatusActivity.class).putExtras(bundle), 102);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean flag = false;
        if (data != null && data.getIntExtra("code", 0) == 101) {
            flag = true;
        }
        if (data != null && data.getIntExtra("code", 0) == 103) {
            android.util.Log.d("System out", "onactivityResult, CallListFragment");

            Intent intent = new Intent();
            getActivity().setResult(0, intent);
            getActivity().finish();
        }
        if (/*requestCode == 101 || */flag == true) {
            mPage = 1;
            mCallList.clear();
            mCallList = new ArrayList<>();
            callAPI_RequestCall();
        }
    }
}
