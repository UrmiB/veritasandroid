package com.veritas.constants;

import android.os.Environment;

public class Constants {

	/* Storage files directory */
	public static String APP_HOME = Environment.getExternalStorageDirectory()
			.getPath() + "/VeritasApp";
	public static String DIR_LOG = APP_HOME + "/Log";
	public static String LOG_ZIP = APP_HOME + "/VeritasApp.zip";
	public static String DIR_IMAGES = APP_HOME + "/data";
	public static String NAME_LOG = "VeritasApp_Log_";
	public static String PREF_FILE = "PREF_VeritasApp";
	

	public static String FRAG_TAG_DASHBOARD = "fragment_dashboard";
	public static String FRAG_TAG_CHANGEPASS = "fragment_change_password";
	public static String FRAG_TAG_CALLLIST = "fragment_calllist";
	public static String FRAG_TAG_NOTIF_LIST = "fragment_notification_list";

}
