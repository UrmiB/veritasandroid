package com.veritas.constants;

/**
 * Created by Urmila on 7/4/16.
 */
public class URLConstant
{
    /**demo URL*/
//    public static String BASEURL="http://veritas.demo.brainvire.com/api/";
//    public static String BASEURL="http://veritasdev.demo.brainvire.com/api/";
    /**Live URL*/
   // public static String BASEURL="http://trail.veritasinfratech.com/app_dev.php/api/";
   // public static String BASEURL="http://trail.veritasinfratech.com/api/";

//    public static String BASEURL="http://trial.veritasinfratech.com/api/";
    public static String BASEURL="http://vimsys.veritasinfratech.com/api/";


    // User Module(LOGIN, SIGNUP & FORGOT Password) APIs
    public static String LOGIN_URL=BASEURL+"login";
    public static String CHANGE_PASS_URL=BASEURL+"change-password";
    public static String FORGOTPASSWORD_URL=BASEURL+"forgot-password";
    public static String DASHBOARD_URL=BASEURL+"dashboard";
    public static String CALLLIST_URL=BASEURL+"request-call";
    public static String GET_COMMENT_URL=BASEURL+"get-callrequest-comments";
    public static String ADD_COMMENT_URL=BASEURL+"add-callrequest-comments";
    public static String SUBMIT_STATUS_URL=BASEURL+"submit-call-status";
    public static String NOTIFICATION_LIST_URL=BASEURL+"get-notification";
    public static String IMAGE_UPLOAD_URL=BASEURL+"uploaderrorimage";


}
