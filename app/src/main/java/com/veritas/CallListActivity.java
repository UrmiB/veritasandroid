package com.veritas;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.veritas.constants.Constants;
import com.veritas.fragment.CallListFragment;
import com.veritas.utils.Log;

public class CallListActivity extends AppCompatActivity {

    Toolbar mToolbar;
    CallListFragment fragment;
    FragmentManager mFrgFragmentManager;
    private String mType="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_list);

        mType = getIntent().getStringExtra("call_type");

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar();

        mFrgFragmentManager = getSupportFragmentManager();

        fragment = CallListFragment.newInstance(mType);
        mFrgFragmentManager.beginTransaction().replace(R.id.id_fragment_holder_call, fragment, Constants.FRAG_TAG_CALLLIST).commit();

    }

    private void initToolbar() {
        switch (mType){
            case "assinged_cur_day":
                setTitle(getResources().getString(R.string.title_assigned));
                break;
            case "resolved_curr_day":
                setTitle(getResources().getString(R.string.title_resolved));
                break;
            case "total_open_call":
                setTitle(getResources().getString(R.string.title_total_open));
                break;
        }

        setSupportActionBar(mToolbar);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    public void onGroupItemClick(MenuItem item) {
        // One of the group items (using the onClick attribute) was clicked
        // The item parameter passed here indicates which item it is
        // All other menu item clicks are handled by onOptionsItemSelected()
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Log.print("System out", "Back");
            Intent intent = new Intent();
            setResult(0, intent);
            CallListActivity.this.finish();
            return true;
        }
        if (id == R.id.action_filter) {
            Log.print("System out", "Open Filter");
            fragment.showFilterPopup();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
