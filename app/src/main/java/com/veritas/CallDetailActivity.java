package com.veritas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.veritas.bean.CallListBean;
import com.veritas.constants.Constants;
import com.veritas.constants.URLConstant;
import com.veritas.fragment.ImageDialogFragment;
import com.veritas.http.HttpRequest;
import com.veritas.utils.Log;
import com.veritas.utils.Storage;
import com.veritas.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CallDetailActivity extends AppCompatActivity {

    private final String SERVICE_TYPE = "Service Type";
    private final String ATM_ID = "ATM ID";
    private final String ATM_LOCATION = "ATM Location";
    private final String HUB = "Hub";
    private final String ERROR_CODE = "Error";
    private final String ERROR_ACTUAL = "Actual Error";
    private final String DATE = "Date";
    private final String ESTIMATE_TAT = "Estimate TAT";
    private final String REMAINING_TAT = "Remaining TAT";
    private final String ACTUAL_TAT = "Actual TAT";
//    private final String SCHEDULED_TAT = "Scheduled TAT";
    private final String NOTES = "Notes";
    private final String STATUS = "Status";
    private final String RESOLUTION_TIME = "Resolution Time";

    Toolbar mToolbar;
    Button txtAddComment, txtChangeStatus, txtUploadPhoto;
    TextView txtCallRequestId;
    LinearLayout mLinearCallDetail;
    NestedScrollView nestedScrollView;

    String callType = "";
    String imagePath = "";
    CallListBean callListBean = null;
    private static final int REQUEST_STORAGE = 0;
    private static final int REQUEST_CAMERA = 3;
    private static final int REQUEST_IMAGE_CAPTURE = REQUEST_STORAGE + 1;
    private Uri cameraImageUri = null;
    ArrayList<String> labelArray = new ArrayList<String>();
    int requestCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_detail);

        initView();
        initToolbar();

        if (getIntent().getExtras() != null) {
            callListBean = (CallListBean) getIntent().getExtras().getSerializable("callDetail");
            callType = getIntent().getStringExtra("callType");
            if (!(callListBean.getReq_image() == null || callListBean.getReq_image().trim().length() == 0))
                txtUploadPhoto.setText("View Photo");
        }

        labelArray.add(SERVICE_TYPE);
        labelArray.add(ATM_ID);
        labelArray.add(ATM_LOCATION);
        labelArray.add(HUB);
        labelArray.add(ERROR_CODE);
        if (callListBean.getCall_status().equalsIgnoreCase("Resolved")) {
            labelArray.add(ERROR_ACTUAL);
        }
        labelArray.add(DATE);
        labelArray.add(ESTIMATE_TAT);

        if (callListBean.getCall_status().equalsIgnoreCase("Assigned")) {
            labelArray.add(REMAINING_TAT);
        }

        if (!callListBean.getCall_status().equalsIgnoreCase("Assigned")) {
            labelArray.add(ACTUAL_TAT);
        }
        if (callListBean.getCall_status().equalsIgnoreCase("Scheduled")) {
//            labelArray.add(SCHEDULED_TAT);
        }

        labelArray.add(NOTES);
        labelArray.add(STATUS);

        //----- Hide/Show buttons based on Call Status
        if (callListBean.getCall_status().equalsIgnoreCase("Closed") || callListBean.getCall_status().equalsIgnoreCase("Resolved")
                || callListBean.getCall_status().equalsIgnoreCase("Rejected") || callListBean.getCall_status().equalsIgnoreCase("Forward to SLM")) {

            labelArray.add(RESOLUTION_TIME);
            txtChangeStatus.setVisibility(View.GONE);

            if (callListBean.getCall_status().equalsIgnoreCase("Closed")) {
                txtAddComment.setVisibility(View.GONE);
                if(callListBean.getReq_image() == null ||callListBean.getReq_image().trim().length() == 0)
                    txtUploadPhoto.setVisibility(View.GONE);
            }
        }

        bindData();

    }

    /**
     * Initialize View component
     */
    private void initView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScroll);
        txtAddComment = (Button) findViewById(R.id.txtAddComment);
        txtAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCommentClick();
            }
        });
        txtChangeStatus = (Button) findViewById(R.id.txtChangeStatus);
        txtChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatusClick();
            }
        });
        txtUploadPhoto = (Button) findViewById(R.id.txtAddPhoto);

        txtUploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callListBean.getReq_image() == null || callListBean.getReq_image().trim().length() == 0)
                    openCamera();
                else {
                    viewPhoto();
                }
            }
        });
        mLinearCallDetail = (LinearLayout) findViewById(R.id.linearCallDetail);
        txtCallRequestId = (TextView) findViewById(R.id.txtCallRequestId);
    }

    public void viewPhoto() {
        FragmentManager fm = getSupportFragmentManager();
        ImageDialogFragment dialog = new ImageDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("image_url", callListBean.getReq_image());

        dialog.setArguments(bundle);
        dialog.show(fm, "Dialog Fragment");
    }

    /**
     * Initialize toolbar
     */

    private void initToolbar() {
        mToolbar.setTitle("Call Detail View");
        setSupportActionBar(mToolbar);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void bindData() {
        txtCallRequestId.setText(getResources().getString(R.string.call_request_id) + " " + callListBean.getCall_request_no());
        mLinearCallDetail.removeAllViews();
        int count = 0;
        for (int i = 0; i < labelArray.size(); i++) {
            LayoutInflater inflater = this.getLayoutInflater();
            View convertView = inflater.inflate(R.layout.item_call_detail, null, false);
            // well set up the ViewHolder
            TextView txtLabelTitle = (TextView) convertView.findViewById(R.id.txtLableTitle);
            TextView txtLabelValue = (TextView) convertView.findViewById(R.id.txtLableValue);
            LinearLayout linearSubHeader = (LinearLayout) convertView.findViewById(R.id.linearCallDetailSubHeader);


            String value = getValue(labelArray.get(i));
            if (labelArray.get(i).equalsIgnoreCase(DATE) || labelArray.get(i).equalsIgnoreCase(RESOLUTION_TIME)) {
                value = value.replace("am", "AM").replace("pm", "PM");
            }
            if (Utils.isNotNull(value)) {
                txtLabelTitle.setText(labelArray.get(i));
                txtLabelValue.setText(value);
                if (count % 2 == 0) {
                    linearSubHeader.setBackgroundColor(Color.parseColor("#E4F2FD"));
                } else {
                    linearSubHeader.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                count++;
                mLinearCallDetail.addView(convertView);
            }
        }

    }

    /**
     * get Label value for call Detail
     */
    private String getValue(String label) {
        String value = "";
        switch (label) {
            case SERVICE_TYPE:
                value = callListBean.getService_type();
                break;
            case ATM_ID:
                value = callListBean.getAtm_id();
                break;
            case ATM_LOCATION:
                value = callListBean.getAtm_location();
                break;
            case HUB:
                value = callListBean.getHub();
                break;
            case ERROR_CODE:
                value = callListBean.getError_code();
                break;
            case ERROR_ACTUAL:
                value = callListBean.getActual_error_code();
                break;
            case DATE:
                value = callListBean.getCreated_date();
                /*String date = "";
                try {
                    date = Utils.convertDateFormate(value, "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy hh:mm a");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                value = date;*/
                break;
            case ESTIMATE_TAT:
                value = callListBean.getEstimated_tat();
                break;
            case REMAINING_TAT:
                value = callListBean.getRemain_tat();
                break;
            case ACTUAL_TAT:
                value = callListBean.getActual_tat();
                break;
//            case SCHEDULED_TAT:
//                value = callListBean.getScheduled_tat();
//                break;
            case NOTES:
                value = callListBean.getNotes().trim();
                break;
            case STATUS:
                value = callListBean.getCall_status();
                break;
            case RESOLUTION_TIME:
                value = callListBean.getResolution_time();
                /*String date1 = "";
                try {
//                    07\/10\/2016 05:41:33 PM
                    date1 = Utils.convertDateFormate(value, "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy hh:mm a");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                value = date1;*/
                break;
        }
        return value;
    }

    /***
     * Add Comment click event
     */
    private void addCommentClick() {
        Utils.ButtonClickEffect(txtAddComment);
        Bundle bundle = new Bundle();
        bundle.putString("callRequestId", callListBean.getCall_request_id());
        startActivity(new Intent(CallDetailActivity.this, AddCommentActivity.class).putExtras(bundle));
    }

    /**
     * Change Status click event
     */
    private void changeStatusClick() {
        Utils.ButtonClickEffect(txtChangeStatus);
        Bundle bundle = new Bundle();
        bundle.putString("callRequestId", callListBean.getCall_request_id());
        bundle.putString("callStatus", callListBean.getCall_status());
        bundle.putSerializable("callDetail", callListBean);
//        bundle.putString("callErrorId",callListBean.getCall_status());
        startActivityForResult(new Intent(CallDetailActivity.this, ChangeStatusActivity.class).putExtras(bundle), 102);
//        startActivity(new Intent(CallDetailActivity.this, ChangeStatusActivity.class).putExtras(bundle));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra("code", requestCode);
                setResult(requestCode, intent);
                finish();
//                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("code", requestCode);
        setResult(requestCode, intent);
        finish();
//        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    /**
     * This checks to see if there is a suitable activity to handle the {@link MediaStore#ACTION_IMAGE_CAPTURE}
     * intent and returns it if found. {@link MediaStore#ACTION_IMAGE_CAPTURE} is for letting another app take
     * a picture from the camera and store it in a file that we specify.
     *
     * @return A prepared intent if found.
     */
    @Nullable
    private Intent createCameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            return takePictureIntent;
        } else {
            return null;
        }
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(CallDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(CallDetailActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
        } else {
            // Eh, prompt anyway
            ActivityCompat.requestPermissions(CallDetailActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(CallDetailActivity.this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(CallDetailActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        } else {
            // Eh, prompt anyway
            ActivityCompat.requestPermissions(CallDetailActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        }
    }

    public void openCamera() {
        if (Utils.checkNeedsPermission(CallDetailActivity.this, Manifest.permission.CAMERA)) {
            requestCameraPermission();
        } else if (Utils.checkNeedsPermission(CallDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestStoragePermission();
        } else {
//            showSheetView();
            dispatchTakePictureIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                // Permission denied
                Toast.makeText(CallDetailActivity.this, "Your action can not perform without access to external storage.", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            } else {
                // Permission denied
                Toast.makeText(CallDetailActivity.this, "Your action can not perform without access to camera.", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * This utility function combines the camera intent creation and image file creation, and
     * ultimately fires the intent.
     *
     * @see {@link #createCameraIntent()}
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = createCameraIntent();
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent != null) {
            // Create the File where the photo should go
            try {
                Storage.verifyImagePath();
                File imageFile = File.createTempFile(
                        "RequestImage",  /* prefix */
                        ".jpg",         /* suffix */
                        new File(Constants.DIR_IMAGES)      /* directory */
                );

                // Save a file: path for use with ACTION_VIEW intents
                cameraImageUri = Uri.fromFile(imageFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (IOException e) {
                // Error occurred while creating the File
                genericError("Could not create imageFile for camera");
            }
        }
    }

    private void genericError() {
        genericError(null);
    }

    private void genericError(String message) {
        Toast.makeText(CallDetailActivity.this, message == null ? "Something went wrong." : message, Toast.LENGTH_SHORT).show();
//        Toast.makeText(CallDetailActivity.this, message == null ? "Could not create imageFile for camera." : message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int _requestCode, int resultCode, Intent data) {
        super.onActivityResult(_requestCode, resultCode, data);

        if (_requestCode == REQUEST_IMAGE_CAPTURE) {
            // Do something with imagePath
//            selectedImage = cameraImageUri;
            if (cameraImageUri != null) {
                imagePath = cameraImageUri.getPath();
                Log.print("System out", "image:  " + imagePath);
                // call Upload API
                try {
                    Bitmap bmp = BitmapFactory.decodeFile(imagePath);

                    Log.print("System out", "size:  height" + bmp.getHeight()+"::width" + bmp.getWidth());
                    bmp = Utils.resize(bmp, 800,800);
                    Log.print("System out", "size:  height" + bmp.getHeight()+"::width" + bmp.getWidth());
                    //--- Rotate captured image file based on Orientation
                    if (bmp != null && Utils.isNotNull(imagePath)) {
                        ExifInterface ei = new ExifInterface(imagePath);
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                        Matrix matrix = new Matrix();

                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                matrix.postRotate(90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                matrix.postRotate(180);
                                break;

                        }

                        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                        FileOutputStream fOut;

                        fOut = new FileOutputStream(imagePath);
                        bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                        fOut.flush();
                        fOut.close();

                        //----- Call Image upload api
                        callUploadImageApi();

                        bmp.recycle();
                    }

                } catch (FileNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {
                genericError();
            }
        } else {
            if (data != null && data.getIntExtra("code", 0) == 101) {
                requestCode = 101;
            } else if (data != null && data.getIntExtra("code", 0) == 103) {
                Log.debug("System out", "OnActivityResult++callDetail");
                requestCode = 103;
                Intent intent = new Intent();
                intent.putExtra("code", requestCode);
                setResult(requestCode, intent);
                CallDetailActivity.this.finish();
            } else {
                requestCode = 0;
            }
        }
    }

    /**
     * Upload Image Api method
     */
    private void callUploadImageApi() {
        try {
            if (Utils.isOnline(this)) {

                List<NameValuePair> objvaluepair = new ArrayList<NameValuePair>();
                objvaluepair.add(new BasicNameValuePair("call_request_id", callListBean.getCall_request_id()));

                new HttpRequest(this, URLConstant.IMAGE_UPLOAD_URL,
                        objvaluepair, true, true, new File(imagePath), "req_image", new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        try {
                            // Parse Api Response
                            if (response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {

                                    Utils.showSnackbar(CallDetailActivity.this, jResponse.optString("message"), nestedScrollView);

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            requestCode = 103;
                                            Intent intent = new Intent();
                                            intent.putExtra("code", requestCode);
                                            setResult(requestCode, intent);
                                            CallDetailActivity.this.finish();
                                        }
                                    }, 2000);

                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(CallDetailActivity.this, jResponse.optString("message"), null);
                                }
                            } else
                                Utils.showSnackbar(CallDetailActivity.this, getResources().getString(R.string.error_server), nestedScrollView);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute();

            } else {
                Utils.showSnackbar(CallDetailActivity.this, getResources().getString(R.string.error_no_network), nestedScrollView);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
