package com.veritas.bean;

import java.io.Serializable;

/**
 * Created by jay on 28/4/16.
 */
public class ErrorCodeBean implements Serializable {
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    private String ID;

    public String getErrorName() {
        return errorName;
    }

    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    private String errorName;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
