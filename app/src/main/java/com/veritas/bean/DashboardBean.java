package com.veritas.bean;

/**
 * Created by urmila.bhanani on 4/12/2016.
 */
public class DashboardBean {
    private String assinged_cur_day;
    private String resolved_curr_day;
    private String total_open_call;
    private String filter_services;
    private String filter_error_code;

    public String getAssinged_cur_day() {
        return assinged_cur_day;
    }

    public void setAssinged_cur_day(String assinged_cur_day) {
        this.assinged_cur_day = assinged_cur_day;
    }

    public String getResolved_curr_day() {
        return resolved_curr_day;
    }

    public void setResolved_curr_day(String resolved_curr_day) {
        this.resolved_curr_day = resolved_curr_day;
    }

    public String getTotal_open_call() {
        return total_open_call;
    }

    public void setTotal_open_call(String total_open_call) {
        this.total_open_call = total_open_call;
    }

    public String getFilter_services() {
        return filter_services;
    }

    public void setFilter_services(String filter_services) {
        this.filter_services = filter_services;
    }

    public String getFilter_error_code() {
        return filter_error_code;
    }

    public void setFilter_error_code(String filter_error_code) {
        this.filter_error_code = filter_error_code;
    }

}
