package com.veritas.bean;

import java.io.Serializable;

/**
 * Created by urmila.bhanani on 4/14/2016.
 */
public class CallListBean implements Serializable{

    private String call_request_id;
    private String call_request_no;
    private String service_type;
    private String atm_id;
    private String atm_location;
    private String hub;
    private String error_code;
    private String error_code_id;
    private String req_image;
    public String actual_error_code;
    private String error_details;
    private String estimated_tat;
    private String actual_tat;
    private String scheduled_tat;
    private String remain_tat;
    private String notes;
    private String call_status;
    private String created_date;
    private String resolution_time;

    public CallListBean() {
    }

    public String getCall_request_id() {
        return call_request_id;
    }

    public void setCall_request_id(String call_request_id) {
        this.call_request_id = call_request_id;
    }

    public String getCall_request_no() {
        return call_request_no;
    }

    public void setCall_request_no(String call_request_no) {
        this.call_request_no = call_request_no;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getAtm_id() {
        return atm_id;
    }

    public void setAtm_id(String atm_id) {
        this.atm_id = atm_id;
    }

    public String getAtm_location() {
        return atm_location;
    }

    public void setAtm_location(String atm_location) {
        this.atm_location = atm_location;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_details() {
        return error_details;
    }

    public void setError_details(String error_details) {
        this.error_details = error_details;
    }

    public String getEstimated_tat() {
        return estimated_tat;
    }

    public void setEstimated_tat(String estimated_tat) {
        this.estimated_tat = estimated_tat;
    }

    public String getActual_tat() {
        return actual_tat;
    }

    public void setActual_tat(String actual_tat) {
        this.actual_tat = actual_tat;
    }

    public String getRemain_tat() {
        return remain_tat;
    }

    public void setRemain_tat(String remain_tat) {
        this.remain_tat = remain_tat;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCall_status() {
        return call_status;
    }

    public void setCall_status(String call_status) {
        this.call_status = call_status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getResolution_time() {
        return resolution_time;
    }

    public void setResolution_time(String resolution_time) {
        this.resolution_time = resolution_time;
    }
    public String getActual_error_code() {
        return actual_error_code;
    }

    public void setActual_error_code(String actual_error_code) {
        this.actual_error_code = actual_error_code;
    }

    public String getScheduled_tat() {
        return scheduled_tat;
    }

    public void setScheduled_tat(String scheduled_tat) {
        this.scheduled_tat = scheduled_tat;
    }

    public String getReq_image() {
        return req_image;
    }

    public void setReq_image(String req_image) {
        this.req_image = req_image;
    }

    public String getError_code_id() {
        return error_code_id;
    }

    public void setError_code_id(String error_code_id) {
        this.error_code_id = error_code_id;
    }
}
