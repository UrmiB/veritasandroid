package com.veritas.bean;

/**
 * Created by urmila.bhanani on 4/21/2016.
 */
public class NotificationBean {
    /*{
        "message": "Success",
            "status": "success",
            "notifications": [{
        "title": "ATM Assigned",
                "date": "05/04/2016",
                "description": "Lorem ipsum dolor sit amet"
    }, {
        "title": "ATM UnAssigned",
                "date": "04/04/2016",
                "description": "Lorem ipsum dolor sit amet"
    }]

    }*/
    private String title;
    private String date;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
