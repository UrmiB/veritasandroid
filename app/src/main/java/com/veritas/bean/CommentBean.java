package com.veritas.bean;

import java.io.Serializable;

/**
 * Created by jay on 18/4/16.
 */
public class CommentBean  implements Serializable{
    private String comment;
    private String name;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String date;
}
