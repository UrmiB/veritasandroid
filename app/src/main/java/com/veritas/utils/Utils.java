package com.veritas.utils;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.veritas.R;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint("NewApi")
public class Utils {
	public final static int COLOR_ANIMATION_DURATION = 1000;
    public final static int DEFAULT_DELAY = 0;

	public static int[][] states = new int[][]{
			new int[]{android.R.attr.state_enabled}, // enabled
			new int[]{-android.R.attr.state_enabled}, // disabled
			new int[]{-android.R.attr.state_checked}, // unchecked
			new int[]{android.R.attr.state_pressed}  // pressed
	};

    /**
     * Change the color of a view with an animation
     *
     * @param v the view to change the color
     * @param startColor the color to start animation
     * @param endColor the color to end the animation
     */
    public static void animateViewColor (View v, int startColor, int endColor) {

        ObjectAnimator animator = ObjectAnimator.ofObject(v, "backgroundColor",
                new ArgbEvaluator(), startColor, endColor);

//        animator.setInterpolator(new PathInterpolator(0.4f,0f,1f,1f));
        animator.setDuration(COLOR_ANIMATION_DURATION);
        animator.start();
    }
	/* Check Internet Connectivity */
	
	public static boolean isOnline(Context context) {

		try {
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			if (cm != null) {
				return cm.getActiveNetworkInfo().isConnectedOrConnecting();
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static void showToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
	public static void showToastLong(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	@SuppressLint("NewApi")
	public static int getDeviceWidth(Context mContext) {

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
			Display display = ((WindowManager) mContext
					.getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int width = size.x;
			return width;
		} else {

			DisplayMetrics displaymetrics = new DisplayMetrics();
			((Activity) mContext).getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);
			int width = displaymetrics.widthPixels;
			return width;

		}

	}

	@SuppressLint("NewApi")
	public static int getDeviceHeight(Context mContext) {

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
			Display display = ((WindowManager) mContext
					.getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int height = size.y;
			return height;

		} else {

			DisplayMetrics displaymetrics = new DisplayMetrics();
			((Activity) mContext).getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			return height;
		}

	}

	public static Bitmap getCircularBitmap(Bitmap bitmap) {
		Bitmap output;

		if (bitmap.getWidth() > bitmap.getHeight()) {
			output = Bitmap.createBitmap(bitmap.getHeight(),
					bitmap.getHeight(), Config.ARGB_8888);
		} else {
			output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(),
					Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		float r = 0;

		if (bitmap.getWidth() > bitmap.getHeight()) {
			r = bitmap.getHeight() / 2;
		} else {
			r = bitmap.getWidth() / 2;
		}

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(r, r, r, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	public static Bitmap cropImage(Bitmap bitMap, int Width, int Height) {
		int bitWidth = bitMap.getWidth();
		int bitHeight = bitMap.getHeight();

		int X = 0;
		int Y = 0;

		if (bitHeight < bitWidth) {
			X = (bitWidth / 2) - (Width / 2);
		} else {
			Y = (bitHeight / 2) - (Height / 2);
		}

		if ((X + Width) <= bitWidth && (Y + Height) <= bitHeight)
			return Bitmap.createBitmap(bitMap, X, Y, Width, Height);
		else
			return bitMap;
	}

	public static Bitmap resize(Bitmap bitMap, int width, int height) {

		int per;
		int bitWidth = bitMap.getWidth();
		int bitHeight = bitMap.getHeight();

		if (bitHeight < bitWidth) {
			per = (height * 100) / bitHeight;
			bitHeight = height;
			bitWidth = (bitWidth * per) / 100;
		} else {
			per = (width * 100) / bitWidth;
			bitWidth = width;
			bitHeight = (bitHeight * per) / 100;
		}
		return Bitmap.createScaledBitmap(bitMap, bitWidth, bitHeight, false);
	}

	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/* Return LastSeen with Date&Time */

	public static String getLastSeenText(long last_seen) {

		String LAST_SEEN_STRING = "";

		long todays_start_millis = Utils.convertStringToDate(
				Utils.convertDateToString(new Date(), "dd/MM/yyyy"),
				"dd/MM/yyyy").getTime();

		long todays_end_millis = todays_start_millis + 1000 * 60 * 60 * 24;

		long yesterday_start_millis = todays_start_millis - 1000 * 60 * 60 * 24;

		long yesterday_end_millis = todays_start_millis - 1000;

		long currMillis = new Date().getTime();

		if (last_seen < currMillis + 1000 * 30
				&& last_seen > currMillis - 1000 * 30) {
			LAST_SEEN_STRING = "Online";
		} else if (last_seen > todays_start_millis
				&& last_seen < todays_end_millis) {
			LAST_SEEN_STRING = "Last Seen Today at "
					+ Utils.millisToDate(last_seen, "hh:mm aa");
		} else if (last_seen > yesterday_start_millis
				&& last_seen < yesterday_end_millis) {
			LAST_SEEN_STRING = "Last Seen Yesterday at "
					+ Utils.millisToDate(last_seen, "hh:mm aa");
		} else {
			LAST_SEEN_STRING = "Last Seen at "
					+ Utils.millisToDate(last_seen, "dd/MM/yyyy hh:mm aa");
		}
		Log.print_i("LAST_SEEN_STRING  :: " + LAST_SEEN_STRING);
		return LAST_SEEN_STRING;
	}

	/* Millis To Date */

	public static String millisToDate(long millis, String format) {

		return new SimpleDateFormat(format).format(new Date(millis));
	}

	/* Date to String */

	public static String convertDateToString(Date objDate, String parseFormat) {
		try {
			return new SimpleDateFormat(parseFormat).format(objDate);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/* String to Date */

	public static Date convertStringToDate(String strDate, String parseFormat) {
		try {
			return new SimpleDateFormat(parseFormat).parse(strDate);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/* Milliseconds to date in String */

	public static String getDate(long milliSeconds, String format) {
		// Create a DateFormatter object for displaying date in specified
		// format.
		Date date = new Date(milliSeconds);
		SimpleDateFormat dateformat = new SimpleDateFormat(format);
		System.out.println(dateformat.format(date));
		return dateformat.format(date);
	}

	/* Hide/Close SoftKeyboard */

	public static void hideSoftKeyboard(Activity activity) {

		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
				.getWindowToken(), 0);
	}

	/* Convert Dp to Pixel */

	public static int convertDpToPx(Context context, float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	/**
	 * Copies text into Clip Board
	 * 
	 * @param mContext
	 * @param mText
	 */
	public static void copyTextToClipBoard(Context mContext, String mText) {
		ClipboardManager myClipboard = (ClipboardManager) mContext
				.getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData myClip = ClipData.newPlainText("passwordBoss", mText);
		myClipboard.setPrimaryClip(myClip);
	}

	/*
	 * set ListView height based on height of childen
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
//			Log.print_i("" + totalHeight);
		}

		LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}
	/*
	 * set ListView height based on height of childen
	 */
	public static void setListViewHeightBasedOnChildren(GridView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
		}
		LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ( (listAdapter.getCount()));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	/**
	 * Returns MD5 of a string
	 */
	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			// Now we need to zero pad it if you actually want the full 32
			// chars.
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @author ubbvand2 Himanshu Method use to load Photo of Contact it will
	 *         load Photo Either from Contact _Id or from PhotoId
	 * @param cr
	 * @param id
	 * @param photo_id
	 * @return
	 */
	public static Bitmap loadContactPhoto(ContentResolver cr, long id,
			long photo_id) {

		Uri uri = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, id);
		InputStream input = ContactsContract.Contacts
				.openContactPhotoInputStream(cr, uri);
		if (input != null) {
			return BitmapFactory.decodeStream(input);
		} else {
			// Log.d("PHOTO","first try failed to load photo");

		}

		byte[] photoBytes = null;

		Uri photoUri = ContentUris.withAppendedId(
				ContactsContract.Data.CONTENT_URI, photo_id);

		Cursor c = cr.query(photoUri,
				new String[] { ContactsContract.CommonDataKinds.Photo.PHOTO },
				null, null, null);

		try {
			if (c.moveToFirst())
				photoBytes = c.getBlob(0);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {

			c.close();
		}

		if (photoBytes != null)
			return BitmapFactory.decodeByteArray(photoBytes, 0,
					photoBytes.length);
		else
			// Log.d("PHOTO","second try also failed");
			return null;
	}

	public static Animation expand(final View v, final boolean expand) {
		try {
			Method m = v.getClass().getDeclaredMethod("onMeasure", int.class,
					int.class);
			m.setAccessible(true);
			m.invoke(v,
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
					MeasureSpec.makeMeasureSpec(
							((View) v.getParent()).getMeasuredWidth(),
							MeasureSpec.AT_MOST));
		} catch (Exception e) {
			e.printStackTrace();
		}

		final int initialHeight = v.getMeasuredHeight();

		if (expand) {
			v.getLayoutParams().height = 0;
			v.setVisibility(View.GONE);
		} else {
			v.getLayoutParams().height = initialHeight;
			v.setVisibility(View.VISIBLE);
		}

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				int newHeight = 0;

				if (expand) {
					newHeight = (int) (initialHeight * interpolatedTime);
				} else {
					newHeight = (int) (initialHeight * (1 - interpolatedTime));
				}

				v.getLayoutParams().height = newHeight;
				v.requestLayout();

				if (interpolatedTime == 1 && !expand)
					v.setVisibility(View.GONE);
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

//		a.setDuration(1000);// SPEED_ANIMATION_TRANSITION

		a.setDuration((int) (initialHeight / v.getContext().getResources()
				.getDisplayMetrics().density));// SPEED_ANIMATION_TRANSITION
		v.startAnimation(a);

		return a;
	}


	public static void expand(final View v) {

		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final int measuredHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1
						? LayoutParams.WRAP_CONTENT
						: (int) (measuredHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp per milliseconds
		a.setDuration((int) (measuredHeight / v.getContext().getResources().getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp per milliseconds
		a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static boolean isNotNullOrBlank(String mString) {
		if (mString==null) {
			return false;
		}
		else if (mString.equalsIgnoreCase("")) {
			return false;
		}else if (mString.equalsIgnoreCase("null")) {
			return false;
		} else {
			return true;
		}
	}
	public static String convertDateFormate(String dateString,
			String inputDateFormat, String outputDateFormat) {
		Date date;
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(inputDateFormat);
		try {
			date = dateFormatLocal.parse(dateString);
			return new SimpleDateFormat(outputDateFormat).format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "";
		}
	}

	public static String getCurrentDate() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	

	public static String getDataColumn(Context context, Uri uri,
			String selection, String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = { column };

		try {
			cursor = context.getContentResolver().query(uri, projection,
					selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(column_index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri
				.getAuthority());
	}
	/*Function for use enlarge hit area for an Imageview*/
	public static void setEnlargeHitArea(final ImageView mButton) {
		try
		{
			final View parent = (View) mButton.getParent();  // button: the view you want to enlarge hit area
			parent.post( new Runnable() {
			    public void run() { 
			        final Rect rect = new Rect(); 
			        mButton.getHitRect(rect); 
			        rect.top -= 100;    // increase top hit area
			        rect.left -= 10;   // increase left hit area
			        rect.bottom += 100; // increase bottom hit area           
			        rect.right += 10;  // increase right hit area
			        parent.setTouchDelegate( new TouchDelegate( rect , mButton)); 
			    } 
			});
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*Function for use enlarge hit area for an TextView*/
	public static void setEnlargeHitArea(final TextView mButton) {
		try
		{
			final View parent = (View) mButton.getParent();  // button: the view you want to enlarge hit area
			parent.post( new Runnable() {
			    public void run() { 
			        final Rect rect = new Rect(); 
			        mButton.getHitRect(rect); 
			        rect.top -= 100;    // increase top hit area
			        rect.left -= 50;   // increase left hit area
			        rect.bottom += 100; // increase bottom hit area           
			        rect.right += 50;  // increase right hit area
			        parent.setTouchDelegate( new TouchDelegate( rect , mButton)); 
			    } 
			});
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	public interface CustomAlertDialogControles {
		public void onOkButtonClickListener();
	}
	
	public interface CustomAlertDialogTwoButtons{
		public void leftButtonClickEvent();
		public void rightButtonClickEvent();
		
	}
	/*for open other application*/
	public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
		if (i == null) {
		    return false;
		    //throw new PackageManager.NameNotFoundException();
		}
		i.addCategory(Intent.CATEGORY_LAUNCHER);
		context.startActivity(i);
		return true;
    }

	/**
	 * Set Ripple effect to view
	 * param mContext: context of screen
	 * param view: View
	 */
	public static void setRippleEffect(Context mContext, AppCompatButton view) {
		int[] colors = new int[]{
				mContext.getResources().getColor(R.color.colorAccent, null),
				mContext.getResources().getColor(R.color.colorAccent, null),
				Color.GREEN,
				Color.WHITE
		};
		ColorStateList myList = new ColorStateList(Utils.states, colors);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			view.setSupportBackgroundTintList(myList);
		} else {
			view.setSupportBackgroundTintList(myList);
			ViewCompat.setBackgroundTintList(view, myList);
		}
	}
	/**
	 * Set Ripple effect to view
	 * param mContext: context of screen
	 * param view: View
	 */
	public static void setRippleEffect(Context mContext, android.support.design.widget.FloatingActionButton view) {
		int[] colors = new int[]{
				mContext.getResources().getColor(R.color.colorPrimary, null),
				mContext.getResources().getColor(R.color.colorPrimary, null),
				mContext.getResources().getColor(R.color.colorPrimaryDark, null),
				Color.WHITE
		};
		ColorStateList myList = new ColorStateList(Utils.states, colors);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			view.setBackgroundTintList(myList);
		} else {
			view.setBackgroundTintList(myList);
			ViewCompat.setBackgroundTintList(view, myList);
		}
	}

	public static void showSnackbar(Context context, String message, View view) {
		Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
		View snackbarView = snackbar.getView();
		TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
		textView.setMaxLines(4);  // show multiple line
		snackbar.show();
	}

	public static void showSnackbarShort(Context context, String message, View view) {
		Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
		View snackbarView = snackbar.getView();
		TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
		textView.setMaxLines(4);  // show multiple line
		snackbar.show();
	}
	public static void ButtonClickEffect(final View v) {
		AlphaAnimation obja = new AlphaAnimation(1.0f, 0.3f);
		obja.setDuration(5);
		obja.setFillAfter(false);
		v.startAnimation(obja);

	}
	public static void showAlertDialog(Context context, String mMsgString) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
		builder.setTitle(context.getResources().getString(R.string.app_name));
		builder.setMessage(mMsgString);
		builder.setPositiveButton("OK", null);
		builder.setNegativeButton("Cancel", null);
		AlertDialog dialog =  builder.show();
		TextView textView = (TextView) dialog.findViewById(android.R.id.message);
		textView.setTextSize(context.getResources().getDimension(R.dimen.txt_regular));

	}

	public static void showPhotoDialog(Context context, String mPath) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
		builder.setTitle(context.getResources().getString(R.string.app_name));



		AlertDialog dialog =  builder.show();

	}

	public static void showErrorAlertDialog(final Context context, String mMsgString, final View view) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
		builder.setTitle(context.getResources().getString(R.string.app_name));
		builder.setMessage(mMsgString);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (view != null) {
					view.requestFocus();
					showKeyboard(context, view);
				}

			}
		});
		AlertDialog dialog =  builder.show();
		TextView textView = (TextView) dialog.findViewById(android.R.id.message);
		textView.setTextSize(/*context.getResources().getDimension(R.dimen.txt_regular)*/14);


	}
	public static boolean isNotNull(String mString) {
		if (mString == null) {
			return false;
		} else if (mString.equalsIgnoreCase("")) {
			return false;
		} else if (mString.equalsIgnoreCase("N/A")) {
			return false;
		} else if (mString.equalsIgnoreCase("[]")) {
			return false;
		} else if (mString.equalsIgnoreCase("null")) {
			return false;
		}else if (mString.equalsIgnoreCase("{}")) {
			return false;
		}
		else {
			return true;
		}
	}
	/**
	 * Hide keyboard if user touch outside editText..
	 * param view: parent view
	 * param mContext: context
	 */
	public static void setupOutSideTouchHideKeyboard(final View view, final Context mContext) {

		//Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					InputMethodManager mgr = (InputMethodManager)
							mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
					return false;
				}

			});
		}

		//If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupOutSideTouchHideKeyboard(innerView, mContext);
			}
		}
	}
	public static void showKeyboard(Context context, View v) {
		/*InputMethodManager mgr = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		mgr.showSoftInput(v, 0);*/

		InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.toggleSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);

	}
	public static TextView getEmptyTextView(Context _Context) {
		TextView mTxtView = new TextView(_Context);
		mTxtView.setTextSize(_Context.getResources().getDimension(R.dimen.txt_regular));
		mTxtView.setText("No records found");
		mTxtView.setGravity(Gravity.CLIP_HORIZONTAL);
		mTxtView.setTextColor(_Context.getResources().getColor(R.color.colorPrimary, null));

		return mTxtView;
	}
	/**
	 * for Android M+
	 * check whether permission is enable or not
	 */
	public static boolean checkNeedsPermission(Context mContext, String permission) {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED;
	}
}
