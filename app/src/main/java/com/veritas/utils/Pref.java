package com.veritas.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;

import com.veritas.constants.Constants;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressLint("NewApi")
public class Pref {

	private static SharedPreferences sharedPreferences = null;
	public static String KEY_USERID ="userID";
	public static String KEY_USERNAME ="userName";
	public static String KEY_PASSWORD ="password";
	public static String KEY_CANREMEMBER ="remember";
	public static String KEY_AUTHTOKEN ="authToken";
	public static String KEY_FIRSTNAME ="firstName";
	public static String KEY_LASTNAME ="lastName";
	public static String KEY_PUSH_DEVICE_TOKEN ="token_push";

	public static void openPref(Context context) {

		sharedPreferences = context.getSharedPreferences(Constants.PREF_FILE,
				Context.MODE_PRIVATE);

	}

	public static String getValue(Context context, String key,
			String defaultValue) {
		Pref.openPref(context);
		String result = Pref.sharedPreferences.getString(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, String value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putString(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

	public static boolean getValue(Context context, String key,
			boolean defaultValue) {
		Pref.openPref(context);
		boolean result = Pref.sharedPreferences.getBoolean(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, boolean value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putBoolean(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static int getValue(Context context, String key,
			int defaultValue) {
		Pref.openPref(context);
		int result = Pref.sharedPreferences.getInt(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, int value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putInt(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static void setStringSet(Context _Context, String key,
			Set<String> mSetArray) {

		Pref.openPref(_Context);

		Editor preferenceEditor = Pref.sharedPreferences.edit();
		preferenceEditor.putStringSet(key, mSetArray);
		preferenceEditor.commit();
		preferenceEditor = null;
		Pref.sharedPreferences = null;
	}

	public static Set<String> getStoredPassHistory(Context _Context, String mKey) {

		Pref.openPref(_Context);

		HashSet<String> mSetPassHistory = (HashSet<String>) Pref.sharedPreferences
				.getStringSet(mKey, new HashSet<String>());

		Pref.sharedPreferences = null;

		return mSetPassHistory;
	}
	public static Editor getPrefEditor(Context mContext){
		Pref.openPref(mContext);
		
		Editor mEditor = Pref.sharedPreferences.edit();
		return mEditor;
	}
	public static void setBeanValue(Context context, String key, ArrayList<?> object) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		ObjectOutputStream objectOutput;
		try {
			objectOutput = new ObjectOutputStream(arrayOutputStream);
			objectOutput.writeObject(object);
			byte[] data = arrayOutputStream.toByteArray();
			objectOutput.close();
			arrayOutputStream.close();

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Base64OutputStream b64 = new Base64OutputStream(out, Base64.DEFAULT);
			b64.write(data);
			b64.close();
			out.close();

			prefsPrivateEditor.putString(key, new String(out.toByteArray()));

			prefsPrivateEditor.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}

		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

	public static ArrayList<?> getBeanValue(Context context, String key) {
		Pref.openPref(context);

		byte[] bytes = Pref.sharedPreferences.getString(key, "{}").getBytes();
		if (bytes.length == 0) {
			return null;
		}
		ByteArrayInputStream byteArray = new ByteArrayInputStream(bytes);
		Base64InputStream base64InputStream = new Base64InputStream(byteArray, Base64.DEFAULT);
		ObjectInputStream in = null;
		ArrayList<?> myObject = new ArrayList<>();
		try {
			in = new ObjectInputStream(base64InputStream);
			try {
				myObject = (ArrayList<?>) in.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (in!=null) {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Pref.sharedPreferences = null;
		return myObject;
	}
	public static void setObjectValue(Context context, String key, HashMap<?,?> object) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		ObjectOutputStream objectOutput;
		try {
			objectOutput = new ObjectOutputStream(arrayOutputStream);
			objectOutput.writeObject(object);
			byte[] data = arrayOutputStream.toByteArray();
			objectOutput.close();
			arrayOutputStream.close();

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Base64OutputStream b64 = new Base64OutputStream(out, Base64.DEFAULT);
			b64.write(data);
			b64.close();
			out.close();

			prefsPrivateEditor.putString(key, new String(out.toByteArray()));

			prefsPrivateEditor.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}

		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

	public static HashMap<?,?> getObjectValue(Context context, String key) {
		Pref.openPref(context);

		byte[] bytes = Pref.sharedPreferences.getString(key, "{}").getBytes();
		if (bytes.length == 0) {
			return null;
		}
		ByteArrayInputStream byteArray = new ByteArrayInputStream(bytes);
		Base64InputStream base64InputStream = new Base64InputStream(byteArray, Base64.DEFAULT);
		ObjectInputStream in = null;
		HashMap<?,?> myObject = new HashMap<>();
		try {
			in = new ObjectInputStream(base64InputStream);
			try {
				myObject = (HashMap<?,?>) in.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (in!=null) {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Pref.sharedPreferences = null;
		return myObject;
	}
	public static void clearData(Context context, String key){
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.remove(key);
		prefsPrivateEditor.commit();
	}
}
