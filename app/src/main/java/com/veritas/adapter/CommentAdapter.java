package com.veritas.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veritas.R;
import com.veritas.bean.CommentBean;
import com.veritas.utils.Utils;

import java.util.ArrayList;

/**
 * Created by jay on 15/4/16.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder > {


    Context mContext;
    ArrayList<CommentBean>commentList;

    public CommentAdapter(Context _context, ArrayList<CommentBean> _Array) {
        mContext = _context;
        commentList= _Array;
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_comment, null);
        return new CommentHolder(view);

    }

    @Override
    public void onBindViewHolder(final CommentHolder holder, int position) {
        try {
            CommentBean bean = commentList.get(position);
            holder.txtUserName.setText(bean.getName());
            String date = "";
            try {
                date = Utils.convertDateFormate(bean.getDate(), "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy hh:mm a");
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.txtCommentDate.setText(date);
            holder.txtComment.setText(bean.getComment());


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }


    public class CommentHolder extends RecyclerView.ViewHolder {

        public TextView txtUserName, txtCommentDate, txtComment;
        LinearLayout linearMain;
        public CommentHolder(View itemView) {
            super(itemView);
            txtUserName = (TextView)itemView.findViewById(R.id.txtUserName);
            txtComment = (TextView)itemView.findViewById(R.id.txtComment);
            txtCommentDate = (TextView)itemView.findViewById(R.id.txtCommentDate);
            linearMain = (LinearLayout)itemView.findViewById(R.id.linearMain);
        }
    }
}