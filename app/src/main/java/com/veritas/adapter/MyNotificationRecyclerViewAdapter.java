package com.veritas.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.veritas.R;
import com.veritas.bean.NotificationBean;
import com.veritas.fragment.NotificationListFragment;
import com.veritas.utils.Utils;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link com.veritas.bean.NotificationBean} and makes a call to the
 * specified {@link NotificationListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyNotificationRecyclerViewAdapter extends RecyclerView.Adapter<MyNotificationRecyclerViewAdapter.ViewHolder> {

    private final List<NotificationBean> mValues;
    private final NotificationListFragment.OnListFragmentInteractionListener mListener;

    public MyNotificationRecyclerViewAdapter(List<NotificationBean> items, NotificationListFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_notification_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTv_title.setText(mValues.get(position).getTitle());
        holder.mTv_desc.setText(Html.fromHtml(mValues.get(position).getDescription()));

        String date = "";
        try {
            date = Utils.convertDateFormate(mValues.get(position).getDate(), "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy hh:mm a");
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.mTv_date.setText(date.replace("am", "AM").replace("pm", "PM"));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTv_title;
        public final TextView mTv_desc;
        public final TextView mTv_date;
        public NotificationBean mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTv_title = (TextView) view.findViewById(R.id.tv_title_notif);
            mTv_desc = (TextView) view.findViewById(R.id.tv_desc_notif);
            mTv_date = (TextView) view.findViewById(R.id.tv_date_notif);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTv_desc.getText() + "'";
        }
    }
}
