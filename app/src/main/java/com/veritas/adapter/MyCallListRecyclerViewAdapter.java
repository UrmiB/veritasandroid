package com.veritas.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.veritas.AddCommentActivity;
import com.veritas.R;
import com.veritas.bean.CallListBean;
import com.veritas.utils.Utils;

import java.util.ArrayList;

/**
 * {@link RecyclerView.Adapter} that can display a Strings.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyCallListRecyclerViewAdapter extends RecyclerView.Adapter<MyCallListRecyclerViewAdapter.ViewHolder> {

    private ArrayList<CallListBean> mCallList = new ArrayList<CallListBean>();
    private Context mContext;
    String callType;
    OnStatusClickListener listener;
    public MyCallListRecyclerViewAdapter(Context mContext, ArrayList<CallListBean> items, String _callType) {
        mCallList = items;
        this.mContext=mContext;
        callType = _callType;
    }

    public void setOnStatusClickListener(OnStatusClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_calllist_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mTv_requestId.setText(mContext.getResources().getString(R.string.call_request_id)+" "+mCallList.get(position).getCall_request_no());
        holder.mTv_Type.setText(mCallList.get(position).getService_type());
        holder.mTv_Error.setText(mCallList.get(position).getError_code());
        holder.mTv_atmID.setText(mCallList.get(position).getAtm_id());
        holder.mTv_atmLoc.setText(mCallList.get(position).getAtm_location());
        String date = "";
        /*try {
            date = Utils.convertDateFormate(mCallList.get(position).getCreated_date(), "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy hh:mm a");
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        if (Utils.isNotNull(mCallList.get(position).getCreated_date())){
            date = mCallList.get(position).getCreated_date();
        }
        holder.mTv_date.setText(date);
        holder.mTv_estimate.setText(mCallList.get(position).getEstimated_tat());

        if (mCallList.get(position).getCall_status().equalsIgnoreCase("Closed")){
            holder.mTv_changeStatus.setVisibility(View.GONE);
            holder.mTv_addComment.setVisibility(View.GONE);
        }else if (mCallList.get(position).getCall_status().equalsIgnoreCase("Resolved")|| mCallList.get(position).getCall_status().equalsIgnoreCase("Rejected")|| mCallList.get(position).getCall_status().equalsIgnoreCase("Forward to SLM")){
            holder.mTv_changeStatus.setVisibility(View.GONE);
        }
        else
        {
            holder.mTv_changeStatus.setVisibility(View.VISIBLE);
        }
        holder.mTv_ViewCall.setTag(position+"");
        holder.mTv_ViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.ButtonClickEffect(holder.mTv_ViewCall);
                CallListBean bean = mCallList.get(Integer.parseInt(v.getTag().toString()));
                listener.onButtonClick(mContext, bean, "viewCall");
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("callDetail", bean);
//                bundle.putString("callType", callType);
//                mContext.startActivity(new Intent(mContext, CallDetailActivity.class).putExtras(bundle));
            }
        });

        holder.mTv_addComment.setTag(position+"");
        holder.mTv_addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.ButtonClickEffect(holder.mTv_addComment);
                Bundle bundle = new Bundle();
                CallListBean bean = mCallList.get(Integer.parseInt(v.getTag().toString()));
                bundle.putSerializable("callRequestId", bean.getCall_request_id());
                mContext.startActivity(new Intent(mContext, AddCommentActivity.class).putExtras(bundle));
            }
        });
        holder.mTv_changeStatus.setTag(position+"");
        holder.mTv_changeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.ButtonClickEffect(holder.mTv_changeStatus);
                CallListBean bean = mCallList.get(Integer.parseInt(v.getTag().toString()));
                listener.onButtonClick(mContext, bean, "changeStatus");
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCallList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTv_requestId;
        public final TextView mTv_Type;
        public final TextView mTv_Error;
        public final TextView mTv_atmID;
        public final TextView mTv_atmLoc;
        public final TextView mTv_date;
        public final TextView mTv_estimate;
        public final TextView mTv_ViewCall;
        public final TextView mTv_changeStatus;
        public final TextView mTv_addComment;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTv_requestId = (TextView) view.findViewById(R.id.tv_requestid);
            mTv_Type = (TextView) view.findViewById(R.id.tv_type);
            mTv_Error= (TextView) view.findViewById(R.id.tv_error);
            mTv_atmID= (TextView) view.findViewById(R.id.tv_atmid);
            mTv_atmLoc= (TextView) view.findViewById(R.id.tv_atmid_loc);
            mTv_date= (TextView) view.findViewById(R.id.tv_date);
            mTv_estimate= (TextView) view.findViewById(R.id.tv_estimate);
            mTv_ViewCall= (TextView) view.findViewById(R.id.tv_viewcall);
            mTv_changeStatus= (TextView) view.findViewById(R.id.tv_change_status);
            mTv_addComment= (TextView) view.findViewById(R.id.tv_addcomment);
        }

    }

    public interface OnStatusClickListener{
        void onButtonClick(Context context, CallListBean bean, String type);
    }

}
