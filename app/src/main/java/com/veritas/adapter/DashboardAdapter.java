package com.veritas.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.veritas.CallListActivity;
import com.veritas.DashboardActivity;
import com.veritas.R;
import com.veritas.bean.DashboardBean;
import com.veritas.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a Strings.
 * TODO: Replace the implementation with code for your data type.
 */
public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> implements View.OnClickListener{

    private ArrayList<DashboardBean> mDashboardList = new ArrayList<DashboardBean>();
    private List<String> mCallList = new ArrayList<String>();
    private Context mContext;
    private String mType="";
    FragmentManager mFrgFragmentManager;
    private OnItemClickListener onItemClickListener;

    public DashboardAdapter(Context mContext, ArrayList<DashboardBean> items, FragmentManager mFrgFragmentManager) {
        mDashboardList = items;
        this.mContext=mContext;
        mCallList = Arrays.asList(mContext.getResources().getStringArray(R.array.call_list));
        this.mFrgFragmentManager = mFrgFragmentManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_dashboard_list, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.mView.setTag(position);
        holder.mTv_titleView.setText(mCallList.get(position));
        switch (position){
            case 0:
                if (Utils.isNotNullOrBlank(mDashboardList.get(0).getAssinged_cur_day()))
                    holder.mTv_numberView.setText("Number : "+mDashboardList.get(0).getAssinged_cur_day());
                else
                    holder.mTv_numberView.setText("Number : "+0);
                break;
            case 1:
                if (Utils.isNotNullOrBlank(mDashboardList.get(0).getResolved_curr_day()))
                    holder.mTv_numberView.setText("Number : "+mDashboardList.get(0).getResolved_curr_day());
                else
                    holder.mTv_numberView.setText("Number : "+0);

                break;
            case 2:
                if (Utils.isNotNullOrBlank(mDashboardList.get(0).getTotal_open_call()))
                    holder.mTv_numberView.setText("Number : "+mDashboardList.get(0).getTotal_open_call());
                else
                    holder.mTv_numberView.setText("Number : "+0);

                break;
        }

        holder.mTv_viewAllView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position){
                    case 0:
                        mType = mContext.getResources().getString(R.string.type_assigned_cur_day);
                        break;
                    case 1:
                        mType = mContext.getResources().getString(R.string.type_resolved_cur_day);
                        break;
                    case 2:
                        mType = mContext.getResources().getString(R.string.type_total_open);
                        break;
                }
                Log.d("System out", "Call Type::: "+mType);
                Intent intent = new Intent(mContext, CallListActivity.class);
                intent.putExtra("call_type", mType);
                ((DashboardActivity)mContext).startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCallList.size();
    }

    @Override
    public void onClick(final View v) {
        // Give some time to the ripple to finish the effect
        if (onItemClickListener != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onItemClickListener.onItemClick(v, (int)v.getTag());
                }
            }, 200);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTv_titleView;
        public final TextView mTv_numberView;
        public final TextView mTv_viewAllView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTv_titleView = (TextView) view.findViewById(R.id.tv_title);
            mTv_numberView = (TextView) view.findViewById(R.id.tv_number);
            mTv_viewAllView = (TextView) view.findViewById(R.id.tv_viewall);
        }

    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public interface OnItemClickListener {

        void onItemClick(View view, int viewModel);

    }
}
