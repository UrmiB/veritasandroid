package com.veritas;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veritas.constants.URLConstant;
import com.veritas.crashhandler.ExceptionHandler;
import com.veritas.http.HttpRequest;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.veritas.R.id.et_password;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity{


    // UI references.
    TextInputLayout il_Email, il_Password;
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private LinearLayout mLl_main;
    TextView mTxtForgot;
    CheckBox mCB_remember;
    ImageView imgShow;
    private boolean canRemember = false;

    /**
     * Constants for set as key value
     */
    private final String USERNAME="username";
    private final String PASSWORD="password";
    private final String DEVICE_TOKEN="deviceToken";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private HttpRequest mAuthTask = null;
//    private UserLoginTask mAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

       /* AssetFileDescriptor afd = getAssets().openFd("AudioFile.mp3");
        MediaPlayer player = new MediaPlayer();
        player.setDataSource(afd.getFileDescriptor());
        player.prepare();
        player.start();*/

        initView();

    }

    private void initView() {
        // Set up the login form.
        mLl_main = (LinearLayout)findViewById(R.id.ll_main);
        Utils.setupOutSideTouchHideKeyboard(mLl_main, this);

        il_Email = (TextInputLayout)findViewById(R.id.il_email);
        il_Password= (TextInputLayout)findViewById(R.id.il_pass);

        mEmailView = (EditText) findViewById(R.id.et_email);
        mPasswordView = (EditText) findViewById(et_password);
        mCB_remember = (CheckBox)findViewById(R.id.cb_remember);
        mTxtForgot = (TextView)findViewById(R.id.tv_forgot);

        mTxtForgot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPasswordClick();
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.btn_sign_in);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);

        imgShow = (ImageView)findViewById(R.id.imageShow);
        imgShow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = v.getTag().toString();
                if(tag.equalsIgnoreCase("off"))
                {
                    imgShow.setImageResource(R.drawable.ic_visibility_gray);
                    mPasswordView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mPasswordView.setSelection(mPasswordView.getText().toString().trim().length());
                    v.setTag("on");
                }
                else
                {
                    imgShow.setImageResource(R.drawable.ic_visibility_off_gray);
                    mPasswordView.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mPasswordView.setSelection(mPasswordView.getText().toString().trim().length());
                    v.setTag("off");
                }
            }
        });

        Pref.openPref(this);

        if(!Pref.getValue(this,Pref.KEY_USERNAME, "").isEmpty())
        {
            mEmailView.setText(Pref.getValue(this,Pref.KEY_USERNAME, ""));
            mEmailView.setSelection(mEmailView.getText().toString().trim().length());
        }
        if(!Pref.getValue(this,Pref.KEY_PASSWORD, "").isEmpty())
        {
            mPasswordView.setText(Pref.getValue(this,Pref.KEY_PASSWORD, ""));
        }
        if((Pref.getValue(this,Pref.KEY_CANREMEMBER, false)== true))
        {
            canRemember = (Pref.getValue(this,Pref.KEY_CANREMEMBER, false));
            mCB_remember.setChecked(true);
        }

        /**
         *   Remember checkbox click event
         * */
        mCB_remember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!canRemember)
                {
                    canRemember = true;
                    mCB_remember.setChecked(true);
                }
                else
                {
                    canRemember = false;
                    mCB_remember.setChecked(false);
                }
            }
        });
    }
    /**
     *   Forgot password click event
     * */
    public void forgotPasswordClick()
    {
        Utils.ButtonClickEffect(mTxtForgot);
        startActivity(new Intent(getApplicationContext(),ForgotPassActivity.class));
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();


        if(!checkUserName())
        {
            return;
        }
        else if(!checkPassword())
        {
            return;
        }
        else
        {
            callLoginAPI();
        }
        /*// Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
//            mEmailView.setError(getString(R.string.error_field_required));
            il_Email.setErrorEnabled(true);
            il_Email.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
//            mEmailView.setError(getString(R.string.error_invalid_email));
            il_Email.setErrorEnabled(true);
            il_Email.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }*/





        /*if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callLoginAPI();
        }*/
    }

    private boolean checkUserName()
    {
        String email = mEmailView.getText().toString();

        if (TextUtils.isEmpty(email.trim())) {
//            mEmailView.setError(getString(R.string.error_field_required));
            il_Email.setErrorEnabled(true);
            il_Email.setError(getString(R.string.error_field_required));
            mEmailView.requestFocus();
            return false;
        } /*else if (!email.contains("@") || !email.contains(".")) {
//            mEmailView.setError(getString(R.string.error_invalid_email));
            il_Email.setErrorEnabled(true);
            il_Email.setError(getString(R.string.error_invalid_email));
            mEmailView.requestFocus();
            return false;
        }*/
        else {
            il_Email.setError(null);
            il_Email.setErrorEnabled(false);
            return true;
        }
    }

    private boolean checkPassword()
    {
        String password = mPasswordView.getText().toString();
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
//            mPasswordView.setError(getString(R.string.error_field_required));
            il_Password.setErrorEnabled(true);
            il_Password.setError(getString(R.string.error_field_required));
            mPasswordView.requestFocus();
            return  false;
        }
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
//            mPasswordView.setError(getString(R.string.error_invalid_password));
            il_Password.setErrorEnabled(true);
            il_Password.setError(getString(R.string.error_invalid_password));
            mPasswordView.requestFocus();
            return  false;
        }
        else
        {
            il_Password.setErrorEnabled(false);
            il_Password.setError(null);
            return true;
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 8;
    }

    private void callLoginAPI(){
        if(Utils.isOnline(this)) {
            try {
                String mDeviceToken = Pref.getValue(LoginActivity.this, Pref.KEY_PUSH_DEVICE_TOKEN, "0000");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(USERNAME, mEmailView.getText().toString().trim());
                jsonObject.put(PASSWORD, mPasswordView.getText().toString().trim());
                jsonObject.put(DEVICE_TOKEN, mDeviceToken);

                mAuthTask = new HttpRequest(LoginActivity.this, URLConstant.LOGIN_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask=null;
                        try {
                            // Parse Api Response
                            if(response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    Pref.openPref(LoginActivity.this);
                                    Pref.setValue(LoginActivity.this, Pref.KEY_USERID, jResponse.opt("user_id").toString());
                                    Pref.setValue(LoginActivity.this, Pref.KEY_AUTHTOKEN, jResponse.opt("auth_token").toString());
                                    Pref.setValue(LoginActivity.this, Pref.KEY_FIRSTNAME, jResponse.opt("firstname").toString());
                                    Pref.setValue(LoginActivity.this, Pref.KEY_LASTNAME, jResponse.opt("lastname").toString());

                                    if(canRemember) {
                                        Pref.setValue(LoginActivity.this, Pref.KEY_USERNAME, mEmailView.getText().toString().trim());
                                        Pref.setValue(LoginActivity.this, Pref.KEY_PASSWORD, mPasswordView.getText().toString().trim());
                                        Pref.setValue(LoginActivity.this, Pref.KEY_CANREMEMBER, canRemember);
                                    }
                                    else
                                    {
                                        Pref.setValue(LoginActivity.this, Pref.KEY_USERNAME, "");
                                        Pref.setValue(LoginActivity.this, Pref.KEY_PASSWORD, "");
                                        Pref.setValue(LoginActivity.this, Pref.KEY_CANREMEMBER, canRemember);
                                    }

                                    Utils.showSnackbar(LoginActivity.this, jResponse.optString("message"), mLl_main);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            startDashBoard();
                                        }
                                    },800);
                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(LoginActivity.this, jResponse.optString("message"), null);
                                }
                            }
                            else
                                Utils.showSnackbar(LoginActivity.this, getResources().getString(R.string.error_server), mLl_main);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Utils.showSnackbar(LoginActivity.this, getResources().getString(R.string.error_no_network), mLl_main);
        }
    }

    private void startDashBoard()
    {
        Intent intent =new Intent(this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        LoginActivity.this.finish();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    /*public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }*/
}

