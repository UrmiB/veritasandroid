package com.veritas.gcm;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.veritas.R;
import com.veritas.utils.Utils;


/**
 * Created by Urmila on 20/4/16.
 */
public class MyGCMAlertActivity extends Activity {

    TextView txtMsg;

    TextView txtOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.alert_gcm);

        txtMsg = (TextView)findViewById(R.id.txtMsg);
        txtOk = (TextView)findViewById(R.id.btnOkPopup);

        txtMsg.setText(getIntent().getStringExtra("message"));
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okClickEvent();
            }
        });
    }
    public void okClickEvent()
    {
        Utils.ButtonClickEffect(txtOk);
        finish();
    }
}
