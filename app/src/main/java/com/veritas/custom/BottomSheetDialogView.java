package com.veritas.custom;


import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veritas.R;
import com.veritas.bean.ErrorCodeBean;
import com.veritas.constants.Constants;
import com.veritas.fragment.CallListFragment;
import com.veritas.fragment.DashboardFragment;
import com.veritas.utils.Log;
import com.veritas.utils.Pref;

import java.util.ArrayList;
import java.util.Arrays;

public class BottomSheetDialogView {


    BottomSheetDialog dialog;
    /**
     * remember to call setLocalNightMode for dialog
     *  @param context
     * @param filter_id current filter type id
     * @param mFrgFragmentManager
     */
    public BottomSheetDialogView(final Context context, final int filter_id, final FragmentManager mFrgFragmentManager) {
        dialog = new BottomSheetDialog(context);
        dialog.getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);

        View view = LayoutInflater.from(context).inflate(R.layout.dialo_filter, null);

        LinearLayout mLl_filter_list = (LinearLayout)view.findViewById(R.id.ll_filter_list);
        LinearLayout mLl_filter_edit = (LinearLayout)view.findViewById(R.id.ll_filter_edit);

        TextView textView = (TextView)view.findViewById(R.id.tv_filter_title);
        final TextInputLayout il_filter_edit = (TextInputLayout)view.findViewById(R.id.il_filter_edit);
        final EditText mEt_filterInput = (EditText) view.findViewById(R.id.et_filter_input);
        Button mBtn_done = (Button) view.findViewById(R.id.btn_filter_done);
        mBtn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEt_filterInput.getText().toString().length()<2){
                    il_filter_edit.setErrorEnabled(true);
                    il_filter_edit.setError(context.getString(R.string.error_filter_input));
                }else{
                    dialog.dismiss();
                    try
                    {
            /* Getting FragmentManager */
                        if (mFrgFragmentManager.findFragmentByTag(Constants.FRAG_TAG_CALLLIST)!=null)
                        {
                            CallListFragment detailFragment = (CallListFragment)mFrgFragmentManager.findFragmentByTag(Constants.FRAG_TAG_CALLLIST);
                            detailFragment.updateFilterValues(filter_id, mEt_filterInput.getText().toString());
                        }
                    }
                    catch (Exception e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.bottom_sheet_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        ArrayList<String> mValue = new ArrayList<String>();
        ArrayList<String> mValue_Id = new ArrayList<String>();

        switch (filter_id){
            case 1:
                il_filter_edit.setHint(context.getResources().getString(R.string.filter_reqid_title));
                break;
            case 2:
                textView.setText(context.getResources().getString(R.string.filter_type));
                mValue = (ArrayList<String>) Pref.getBeanValue(context, "filter_type_value_obj");
                mValue_Id = (ArrayList<String>) Pref.getBeanValue(context, "filter_type_key_obj");
                Log.print("System out", "Filter Service:: "+mValue.toString());
                recyclerView.setAdapter(new SimpleAdapter(mValue, mValue_Id, null, mFrgFragmentManager, filter_id));
                break;
            case 3:
                il_filter_edit.setHint(context.getResources().getString(R.string.filter_atmid_title));
                break;
            case 4:
                textView.setText(context.getResources().getString(R.string.filter_error_title));
                ArrayList<ErrorCodeBean> myList =  (ArrayList<ErrorCodeBean>) Pref.getBeanValue(context, "sorted_err_list");
                mValue = (ArrayList<String>) Pref.getBeanValue(context, "filter_err_value_obj");
                mValue_Id = (ArrayList<String>) Pref.getBeanValue(context, "filter_err_key_obj");
                Log.print("System out", "Filter Err:: "+mValue.toString());
                recyclerView.setAdapter(new SimpleAdapter(mValue, mValue_Id,  myList, mFrgFragmentManager, filter_id));
                break;
            case 5:
                textView.setText(context.getResources().getString(R.string.filter_status));
                mValue = new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.status_list)));
//                mValue_Id = (ArrayList<String>) Pref.getBeanValue(context, "filter_err_key_obj");
                Log.print("System out", "Filter Err:: "+mValue.toString());
                recyclerView.setAdapter(new SimpleAdapter(mValue, mValue_Id, null, mFrgFragmentManager, filter_id));
                break;
        }

        if (filter_id==1 || filter_id==3){
            mLl_filter_list.setVisibility(View.GONE);
            mLl_filter_edit.setVisibility(View.VISIBLE);
        }else{
            mLl_filter_list.setVisibility(View.VISIBLE);
            mLl_filter_edit.setVisibility(View.GONE);
        }

        dialog.setContentView(view);
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                il_filter_edit.setErrorEnabled(false);
            }
        });
    }

    public static void show(Context context, int dayNightMode, FragmentManager mFrgFragmentManager) {
        new BottomSheetDialogView(context, dayNightMode, mFrgFragmentManager);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.list_item_text_view);
        }
    }

    public class SimpleAdapter extends RecyclerView.Adapter<ViewHolder> {

        ArrayList<String> mValue;
        ArrayList<String> mValueKey;
        ArrayList<ErrorCodeBean> mBeanList;
        FragmentManager mFrgFragmentManager;
        int filter_id;

        public SimpleAdapter(ArrayList<String> mValue, ArrayList<String> mValue_Id, ArrayList<ErrorCodeBean> mBeanList, FragmentManager mFrgFragmentManager, int filter_id) {
            this.mValue=mValue;
            this.mFrgFragmentManager=mFrgFragmentManager;
            this.filter_id=filter_id;
            this.mValueKey=mValue_Id;
            this.mBeanList=mBeanList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.list_item, null);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mTextView.setText(mValue.get(position).toString().trim());
            holder.mTextView.setTag(position);
            if (filter_id==2){
                if (DashboardFragment.mCheckedArray_type.get(position)){
                    holder.mTextView.setSelected(true);
                }else{
                    holder.mTextView.setSelected(false);
                }
            }else if (filter_id==5){
                if (DashboardFragment.mCheckedArray_status.get(position)){
                    holder.mTextView.setSelected(true);
                }else{
                    holder.mTextView.setSelected(false);
                }
            }else{
                holder.mTextView.setText(mBeanList.get(position).getErrorName());
                if (DashboardFragment.mCheckedArray_err.get(position)){
                    holder.mTextView.setSelected(true);
                }else{
                    holder.mTextView.setSelected(false);
                }
            }

            holder.mTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog!=null){
                        dialog.dismiss();
                    }
//                    Log.print("System out", "textPosition:"+mBeanList.get(Integer.parseInt(v.getTag().toString())).getID());
                    if (filter_id==2){
                        if (DashboardFragment.prevSelectedType!=-1){
                            DashboardFragment.mCheckedArray_type.put(DashboardFragment.prevSelectedType, false);
                        }
                        DashboardFragment.mCheckedArray_type.put((Integer)v.getTag(), true);
                        DashboardFragment.prevSelectedType=(Integer)v.getTag();
                    }else if (filter_id==5){
                        if (DashboardFragment.prevSelectedStatus!=-1){
                            DashboardFragment.mCheckedArray_status.put(DashboardFragment.prevSelectedStatus, false);
                        }
                        DashboardFragment.mCheckedArray_status.put((Integer)v.getTag(), true);
                        DashboardFragment.prevSelectedStatus=(Integer)v.getTag();
                    }else{
                        if (DashboardFragment.prevSelectedErr!=-1){
                            DashboardFragment.mCheckedArray_err.put(DashboardFragment.prevSelectedErr, false);
                        }
                        DashboardFragment.mCheckedArray_err.put((Integer)v.getTag(), true);
                        DashboardFragment.prevSelectedErr=(Integer)v.getTag();
                    }

                        try
                        {
                        /* Getting FragmentManager */
                            if (mFrgFragmentManager.findFragmentByTag(Constants.FRAG_TAG_CALLLIST)!=null)
                            {
                                CallListFragment callFragment = (CallListFragment)mFrgFragmentManager.findFragmentByTag(Constants.FRAG_TAG_CALLLIST);
                                if (filter_id==5){
                                    callFragment.updateFilterValues(filter_id, mValue.get((Integer) v.getTag()));
                                }else if (filter_id==4){
                                    callFragment.updateFilterValues(filter_id, mBeanList.get(Integer.parseInt(v.getTag().toString())).getID());
                                }else{
                                    callFragment.updateFilterValues(filter_id, mValueKey.get((Integer) v.getTag()));
                                }

                            }
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                }
            });
        }

        @Override
        public int getItemCount() {
            if (!mValue.isEmpty()){
                return mValue.size();
            }else{
                return mBeanList.size();
            }
        }
    }
}
