package com.veritas;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.veritas.bean.CallListBean;
import com.veritas.bean.ErrorCodeBean;
import com.veritas.constants.URLConstant;
import com.veritas.http.HttpRequest;
import com.veritas.utils.Log;
import com.veritas.utils.Pref;
import com.veritas.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeStatusActivity extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;
    Toolbar mToolbar;
    EditText edtChangeStatus, edtNote, edtScheduleTime, edtActualErr;
    TextInputLayout il_note;
    Button btnSubmit;
    PopupMenu popupMenu;
    String statusTitle = "", callRequestId = "", mScheduleTAT = "00:00", mErrCode = "";
    HttpRequest mAuthTask;
    PopupWindow popupWindow;
    Point p;
    ArrayList<ErrorCodeBean> mErrorCodeList;
    ArrayList<String> mErrorIDList;
    CallListBean callListBean;
    List<String> contentArray, mErrCodeValueArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_status);

        initView();
        initToolbar();

        if (getIntent().getExtras() != null) {
            callRequestId = getIntent().getStringExtra("callRequestId");
//            statusTitle = getIntent().getStringExtra("callStatus");
            callListBean = (CallListBean) getIntent().getExtras().getSerializable("callDetail");
            if (statusTitle.trim().length() > 0 && !statusTitle.equalsIgnoreCase("Assigned"))
                edtChangeStatus.setText(statusTitle);
        }
        if (statusTitle.equalsIgnoreCase("Scheduled")) {
            edtScheduleTime.setVisibility(View.VISIBLE);
//            edtScheduleTime.setText();
        }

        setStatusArray();

        mErrCode = callListBean.getError_code_id(); // Default value will be error code id..
        mErrorCodeList = (ArrayList<ErrorCodeBean>) Pref.getBeanValue(this, "sorted_err_list");

        mErrCodeValueArray = new ArrayList<>();
        mErrorIDList = new ArrayList<>();
        mErrCodeValueArray.add("NONE");
        mErrorIDList.add("");
        for (int i = 0; i < mErrorCodeList.size(); i++) {
            if (mErrorCodeList.get(i).getType().equalsIgnoreCase(callListBean.getService_type())) {
                mErrCodeValueArray.add(mErrorCodeList.get(i).getErrorName());
                mErrorIDList.add(mErrorCodeList.get(i).getID());
            }
        }
    }

    private void initView() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        il_note = (TextInputLayout) findViewById(R.id.il_note);
        edtChangeStatus = (EditText) findViewById(R.id.edtChangeStatus);
        edtScheduleTime = (EditText) findViewById(R.id.edtChangeStatus_time);
        edtActualErr = (EditText) findViewById(R.id.edtChangeStatus_err);

        edtNote = (EditText) findViewById(R.id.edtNote);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitClick();
            }
        });

        contentArray = Arrays.asList(getResources().getStringArray(R.array.status_to_change));
        final List<String> mScheduleTATArray = Arrays.asList(getResources().getStringArray(R.array.dd_schedule_tat));

        edtChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openStatusPopup();
//                edtChangeStatus.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_up_24dp, 0);
                PopupWindow popUp = popupWindowsort(contentArray, R.string.dd_status);
                popUp.showAsDropDown(edtChangeStatus, 0, 0); //
            }
        });

        edtScheduleTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow popUp = popupWindowsort(mScheduleTATArray, R.string.dd_time);
                popUp.showAsDropDown(edtScheduleTime, 0, 0); //
            }
        });

        edtActualErr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow popUp = popupWindowsort(mErrCodeValueArray, R.string.dd_error);
                popUp.showAsDropDown(edtActualErr, 0, 0); //
            }
        });

    }

    private void setStatusArray() {

//        1. Open, Assigned, WIP, Forward to SLM, Resolved, Closed  [Rejected, Postponed, Scheduled]

        if (callListBean.getCall_status().equalsIgnoreCase("Assigned")) {
            contentArray = new ArrayList<>();
//            contentArray.add("Scheduled");
            contentArray.add("WIP");
//            contentArray.add("Postponed");
            contentArray.add("Forward to SLM");
//            contentArray.add("Rejected");
//            contentArray.add("Resolved");
        }
        else if (callListBean.getCall_status().equalsIgnoreCase("Scheduled")) {
            contentArray = new ArrayList<>();
//            contentArray.add("Scheduled");
            contentArray.add("WIP");
//            contentArray.add("Postponed");
            contentArray.add("Forward to SLM");
//            contentArray.add("Resolved");
        } else if (callListBean.getCall_status().equalsIgnoreCase("Wip")) {
            contentArray = new ArrayList<>();
//            contentArray.add("WIP");
//            contentArray.add("Postponed");
            contentArray.add("Forward to SLM");
            // change 28-11-2016 "resolved" commented
//            contentArray.add("Resolved");
        } else if (callListBean.getCall_status().equalsIgnoreCase("Postponed")) {
            contentArray = new ArrayList<>();
            contentArray.add("WIP");
//            contentArray.add("Postponed");
            contentArray.add("Forward to SLM");
            // change 28-11-2016 "resolved" commented
//            contentArray.add("Resolved");
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        EditText editText = (EditText) findViewById(R.id.edtChangeStatus);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        editText.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }

    /**
     * Initialize toolbar
     */
    private void initToolbar() {
        mToolbar.setTitle("Change Status");
        setSupportActionBar(mToolbar);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(102, intent);
                finish();
//                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(102, intent);
        finish();
//        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private PopupWindow popupWindowsort(final List<String> contentArray, final int mType_dd) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChangeStatusActivity.this, android.R.layout.simple_list_item_1,
                contentArray);
        // the drop down list is a list view
        ListView listViewSort = new ListView(ChangeStatusActivity.this);//(ListView)layout.findViewById(R.id.listView);

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.setMargins(16, 0, 16, 0);
        listViewSort.setLayoutParams(param);
        listViewSort.setSmoothScrollbarEnabled(true);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            listViewSort.setScrollBarSize(5);
        }
        // set our adapter and pass our pop up window contents
        listViewSort.setAdapter(adapter);

        // set on item selected
        listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (mType_dd) {
                    case R.string.dd_status:
                        if (contentArray.get(position).equalsIgnoreCase("Scheduled")) {
                            edtScheduleTime.setVisibility(View.VISIBLE);
                            edtActualErr.setVisibility(View.GONE);
                        } else if (contentArray.get(position).equalsIgnoreCase("Resolved")) {
                            edtScheduleTime.setVisibility(View.GONE);
                            edtActualErr.setVisibility(View.VISIBLE);
                        } else {
                            edtScheduleTime.setVisibility(View.GONE);
                            edtActualErr.setVisibility(View.GONE);
                        }
                        statusTitle = contentArray.get(position);
                        edtChangeStatus.setText(contentArray.get(position));
                        break;
                    case R.string.dd_time:
                        switch (contentArray.get(position)) {
                            case "NONE":
                                mScheduleTAT = "00:00";
                                break;
                            case "30 mins":
                                mScheduleTAT = "00:30";
                                break;
                            case "60 mins":
                                mScheduleTAT = "01:00";
                                break;
                            case "90 mins":
                                mScheduleTAT = "01:30";
                                break;
                            case "120 mins":
                                mScheduleTAT = "02:00";
                                break;
                        }
                        if (position == 0)
                            edtScheduleTime.setText(getResources().getString(R.string.select_schedule));
                        else
                            edtScheduleTime.setText(contentArray.get(position));
                        Log.print("System out", "Time is:::: " + mScheduleTAT);
                        break;
                    case R.string.dd_error:
                        mErrCode = mErrorIDList.get(position);
                        if (position == 0) {
                            edtActualErr.setText(getResources().getString(R.string.select_actual_err));
                            mErrCode = callListBean.getError_code_id();
                        } else
                            edtActualErr.setText(contentArray.get(position));
                        Log.print("System out", "Error is:::: " + mErrCode);
                        break;
                }
                if (popupWindow != null) {
                    popupWindow.dismiss();
//                    edtChangeStatus.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_down_24dp, 0);
                }
            }
        });


        // some other visual settings for popup window
        // initialize a pop up window type
        popupWindow = new PopupWindow(this);
        popupWindow.setFocusable(true);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x - 40;//size.x;
        int height = size.y - 40;//size.y;

        popupWindow.setWidth(width - 10);
        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_background));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the listview as popup content
        popupWindow.setContentView(listViewSort);


        return popupWindow;
    }


    private void submitClick() {
        Utils.ButtonClickEffect(btnSubmit);
        if (statusTitle.trim().length() == 0) {
            Utils.showToast(this, "Change Status field is required field.");
            return;
        } else if (edtNote.getText().length() == 0) {
            il_note.setErrorEnabled(true);
            il_note.setError(getResources().getString(R.string.error_field_required));
            return;
        } else {
            il_note.setErrorEnabled(false);
            callChangeStatusApi(edtNote.getText().toString());
        }
    }

    private void callChangeStatusApi(String text) {
        if (Utils.isOnline(ChangeStatusActivity.this)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ChangeStatusActivity.this, Pref.KEY_USERID, ""));
                jsonObject.put("notes", "Status > " + statusTitle + " : " + text);
                jsonObject.put("call_status", statusTitle);
                jsonObject.put("request_id", callRequestId);
                jsonObject.put("auth_token", Pref.getValue(ChangeStatusActivity.this, Pref.KEY_AUTHTOKEN, ""));
                jsonObject.put("scheduled_tat", mScheduleTAT);
                jsonObject.put("actual_error_code", mErrCode);

                mAuthTask = new HttpRequest(ChangeStatusActivity.this, URLConstant.SUBMIT_STATUS_URL,
                        jsonObject.toString(), true, new HttpRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            // Parse Api Response
                            if (response != null) {
                                JSONObject jResponse = new JSONObject(response);

                                if (jResponse.opt("status").equals("success")) {
                                    Utils.showSnackbar(ChangeStatusActivity.this, jResponse.optString("message"), coordinatorLayout);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent();
                                            intent.putExtra("code", 103);
                                            setResult(103, intent);
                                            finish();
                                        }
                                    }, 4000);
                                } else if (jResponse.opt("status").equals("error")) {
                                    Utils.showErrorAlertDialog(ChangeStatusActivity.this, jResponse.optString("message"), null);
                                }
                            } else
                                Utils.showSnackbar(ChangeStatusActivity.this, getResources().getString(R.string.error_server), coordinatorLayout);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showSnackbar(ChangeStatusActivity.this, getResources().getString(R.string.error_no_network), coordinatorLayout);
        }
    }
}
